using System;
using System.Collections.Generic;

namespace IMS.Api.Entity
{
    public class UploadFileInfo : BaseEntity
    {

        // 原始名称（含扩展名）
        public string OriginName { get; set; }

        // 现在的名称（随机名称，含扩展名）
        public string CurrentName { get; set; }

        // 相对地址
        public string RelativePath { get; set; }
        
        // 文件类型
        public string ContentType { get; set; }

    }
}