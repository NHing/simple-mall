using System;

namespace IMS.Api.Entity
{
    public class Stores : BaseEntity
    {
        // 店铺LogoId
        public int StoreLogoId { get; set; }
        // 店铺名称
        public string StoreName { get; set; }
        // 店铺粉丝数总和
        public int StoreFans { get; set; }
        // 商店创始人
        public string StoreCreator { get; set; }
    }
}