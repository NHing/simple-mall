using System;

namespace IMS.Api.Entity
{
    public class Addresses : BaseEntity
    {
        // 下单用户Id
        public int FromUserId { get; set; }
        // 收货人
        public string Consignee { get; set; }
        // 用户地址
        public string UserAddress { get; set; }
        // 用户电话
        public string TelephoneNumber { get; set; }
    }
}