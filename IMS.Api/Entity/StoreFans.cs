using System;

namespace IMS.Api.Entity
{
    public class StoreFans : BaseEntity
    {
        // 店铺Id
        public int FromStoreId { get; set; }
        // 粉丝用户Id
        public int FromUserId { get; set; }
        // 粉丝关注状态(0--取消关注/1--有效关注)
        public byte FocusStatus { get; set; }
    }
}