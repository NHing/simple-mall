using System;

namespace IMS.Api.Entity
{
    public class Slideshows : BaseEntity
    {
        // 图片Id
        public int PicId { get; set; }
        // 图片名称
        public string PicName { get; set; }
        // 对应商品Id
        public int CommodityId { get; set; }
    }
}