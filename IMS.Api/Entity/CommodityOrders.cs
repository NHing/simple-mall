using System;

namespace IMS.Api.Entity
{
    public class CommodityOrders : BaseEntity
    {
        // 用户Id
        public int FromUserId { get; set; }
        // 用户名
        public string UserName { get; set; }
        // 商品信息
        public string CommodityInfo { get; set; }
        // 收货地址Id
        public int DeliveryAddressId { get; set; }
        // 收货号码
        public string TelephoneNumber { get; set; }
        // 收货人
        public string Consignee { get; set; }
        // 收货地址
        public string DeliveryAddress { get; set; }
        // 支付金额
        public int PaymentAmount { get; set; }
        // 支付方式
        public int PaymentId { get; set; }
        // 支付方式名称
        public string PaymentName { get; set; }
        // 订单号
        public string OrderNumber { get; set; }
        // 结算状态
        public bool SettlementStatus { get; set; }
        // 发货状态状态
        public bool DeliveryStatus { get; set; }
    }
}