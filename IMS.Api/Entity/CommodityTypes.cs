using System;

namespace IMS.Api.Entity
{
    public class CommodityTypes : BaseEntity
    {
        // 商品类型名称
        public string CommodityTypeName { get; set; }
        // 商品类型父级Id
        public int CommodityPid { get; set; }
        // 商品类型层级（0--顶级，1--二级，2--三级）
        public int CommodityLevel { get; set; }
    }
}