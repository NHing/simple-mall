using System;

namespace IMS.Api.Entity
{
    public class Roles : BaseEntity
    {
        // 角色名
        public string RoleName { get; set; }
        // 角色数量总和
        public int UsersCount { get; set; }
        // 角色创建人
        public string Creator { get; set; }
    }
}