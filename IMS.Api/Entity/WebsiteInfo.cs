using System;

namespace IMS.Api.Entity
{
    public class WebsiteInfo : BaseEntity
    {
        // 网站LogoId
        public int WebsiteLogoId { get; set; }
        // 网站名称
        public string WebsiteName { get; set; }
        // 域名
        public string DomainName { get; set; }
        // ICP备案
        public string ICPNumber { get; set; }
        // 公安备案
        public string PoliceNumber { get; set; }
    }
}