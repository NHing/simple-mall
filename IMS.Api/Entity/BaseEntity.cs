using System;

namespace IMS.Api.Entity
{
    public abstract class BaseEntity
    {
        // 主键Id
        public int Id { get; set; }
        // 激活状态
        public bool IsActived { get; set; }
        // 删除状态
        public bool IsDeleted { get; set; }
        // 创建时间
        public DateTime CreatedTime { get; set; }
        // 更新时间
        public DateTime UpdatedTime { get; set; }
        // 备注
        public string Remarks { get; set; }
    }
}