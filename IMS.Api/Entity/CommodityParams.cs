using System;

namespace IMS.Api.Entity
{
    public class CommodityParams : BaseEntity
    {
        // 商品参数名称
        public string CommodityParamName { get; set; }
        // 商品参数对应商品类别Id
        public int CommodityTypesId { get; set; }
        // 商品参数子级
        public string ParamChildren { get; set; }
    }
}