using System;

namespace IMS.Api.Entity
{
    public class ShoppingCarts : BaseEntity
    {
        // 用户Id
        public int FromUserId { get; set; }
        // 商品Id
        public int CommodityId { get; set; }
        // 商品名称
        public string CommodityName { get; set; }
        // 商品价格
        public double CommodityPrice { get; set; }
        // 商品数量
        public int CommodityNum { get; set; }
        // 选中的参数
        public string CommodityParams { get; set; }
        // 商品图片id
        public string CommodityImagesId { get; set; }
        // 送货地址
        public string CommodityAddress { get; set; }
    }
}