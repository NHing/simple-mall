using System;

namespace IMS.Api.Entity
{
    public class Users : BaseEntity
    {
        // 用户名
        public string Username { get; set; }
        // 密码
        public string Password { get; set; }
        // 昵称
        public string Nickname { get; set; }
        // 头像Id
        public int AvatarId { get; set; }
        // 手机号码
        public string TelephoneNumber { get; set; }
        // 用户收货地址
        public string Address { get; set; }
        // 用户角色Id(1--管理员用户，2--供货商，3--普通用户)
        public int RoleId { get; set; }
        // 邮箱
        public string Email { get; set; }
    }
}