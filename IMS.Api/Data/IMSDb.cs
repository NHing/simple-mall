using Microsoft.EntityFrameworkCore;
using IMS.Api.Entity;

namespace IMS.Api.Db
{
    public class IMSDb : DbContext
    {
        // 因为我们使用AddDbContext到容器，所以此处必须得有带参数的构造函数（而且必须传递DbContextOptions类型的参数，同时父类也得调用这个参数）
        public IMSDb(DbContextOptions options) : base(options)
        {

        }
        // 图片上传表
        public DbSet<UploadFileInfo> UploadFileInfos { get; set; }
        // 用户表
        public DbSet<Users> Users { get; set; }
        // 角色表
        public DbSet<Roles> Roles { get; set; }
        // 地址表
        public DbSet<Addresses> Addresses { get; set; }
        // 商品表
        public DbSet<Commodities> Commodities { get; set; }
        // 商品类型表
        public DbSet<CommodityTypes> CommodityTypes { get; set; }
        // 商品参数表
        public DbSet<CommodityParams> CommodityParams { get; set; }
        // 商品图片表
        public DbSet<CommodityPics> CommodityPics { get; set; }
        // 商品订单表
        public DbSet<CommodityOrders> CommodityOrders { get; set; }
        // 支付方式表
        public DbSet<Payments> Payments { get; set; }
        // 购物车详情表
        public DbSet<ShoppingCarts> ShoppingCarts { get; set; }
        // 轮播图表
        public DbSet<Slideshows> Slideshows { get; set; }
        // 商铺表
        public DbSet<Stores> Stores { get; set; }
        // 商铺粉丝表
        public DbSet<StoreFans> StoreFans { get; set; }
        // 网站信息表
        public DbSet<WebsiteInfo> WebsiteInfos { get; set; }
        // 日志审计表
        public DbSet<AuditInfo> AuditInfos { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            // 使用配置文件的方式，将数据库字符串加载进来（见StartUp中注册数据库上下文）
            // builder.UseSqlServer(@"host=8.129.0.19;database=IMSDb;uid=postgres;pwd=a7713085963.;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 初始化几个用户 虽然Id是自动生成，但此处必须明确指定
            modelBuilder.Entity<Users>().HasData(
                new Users
                {
                    Id = 1,
                    Username = "admin1",
                    Password = "123456",
                    Nickname = "超级管理员",
                    TelephoneNumber = "",
                    Email = "",
                    RoleId = 1,
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Users
                {
                    Id = 2,
                    Username = "admin2",
                    Password = "123456",
                    Nickname = "超级管理员",
                    TelephoneNumber = "",
                    Email = "",
                    RoleId = 2,
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Users
                {
                    Id = 3,
                    Username = "admin3",
                    Password = "123456",
                    Nickname = "超级管理员",
                    TelephoneNumber = "",
                    Email = "",
                    RoleId = 3,
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                }
            );
            // base.OnModelCreating(modelBuilder);
            // 初始化几个角色,虽然Id是自动生成，但此处必须明确指定
            modelBuilder.Entity<Roles>().HasData(
                new Roles
                {
                    Id = 1,
                    RoleName = "超级管理员",
                    UsersCount = 1,
                    Creator = "超级管理员",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Roles
                {
                    Id = 2,
                    RoleName = "供货商",
                    UsersCount = 1,
                    Creator = "超级管理员",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Roles
                {
                    Id = 3,
                    RoleName = "普通用户",
                    UsersCount = 1,
                    Creator = "超级管理员",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                }
            );
            // base.OnModelCreating(modelBuilder);
            // 初始化一个网站信息,虽然Id是自动生成，但此处必须明确指定
            modelBuilder.Entity<WebsiteInfo>().HasData(
                new WebsiteInfo
                {
                    Id = 1,
                    WebsiteLogoId = 0,
                    WebsiteName = "待定",
                    DomainName = "待定",
                    ICPNumber = "待定",
                    PoliceNumber = "待定",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                }
            );
            // 初始化几个支付类型,虽然Id是自动生成，但此处必须明确指定
            modelBuilder.Entity<Payments>().HasData(
                new Payments
                {
                    Id = 1,
                    PaymentType = "微信支付",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Payments
                {
                    Id = 2,
                    PaymentType = "支付宝支付",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Payments
                {
                    Id = 3,
                    PaymentType = "银行卡支付",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                },
                new Payments
                {
                    Id = 4,
                    PaymentType = "信用卡支付",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    Remarks = null
                }
            );
            // base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CommodityOrders>().HasData(
                new CommodityOrders
                {
                    Id = 1,
                    FromUserId = 1,
                    UserName = "admin1",
                    CommodityInfo = "1,2",
                    PaymentAmount = 999,
                    PaymentId = 1,
                    DeliveryAddressId = 1,
                    DeliveryAddress = "福建省,莆田市,仙游县 银河系",
                    Consignee = "大佬大佬",
                    TelephoneNumber = "17679963695",
                    PaymentName = "微信支付",
                    OrderNumber = "202108237412",
                    SettlementStatus = false,
                    DeliveryStatus = false,
                    CreatedTime = System.DateTime.Now
                }
            );
            base.OnModelCreating(modelBuilder);
        }
    }
}