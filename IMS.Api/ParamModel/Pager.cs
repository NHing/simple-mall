namespace IMS.Api.ParamModel
{
    public class Pager
    {
        // 页码（当前第几页）o
        public int PageIndex { get; set; }
        // 页大小（一页里面行的数量）
        public int PageSize { get; set; }
        // 总记录数量
        public int PageTotal { get; set; }
        // 查询字段
        public string Query { get; set; }
        // 判断查询时查询方式
        public int QueryType { get; set; }
        // 判断商品查询时查询时商品层级（1--顶级，2--二级，3--三级）
        public int Type { get; set; }
        // 判断是否进入回收站
        public bool Recycle { get; set; }

    }
}