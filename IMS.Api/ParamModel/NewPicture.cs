using System;

namespace IMS.Api.ParamModel
{
    public class NewPicture
    {
        // 图片Id
        public int PicId { get; set; }
        // 上传人Id
        public int FromUserId { get; set; }
        // 图片名称
        public string PicName { get; set; }
        // 对应商品Id
        public int CommodityId { get; set; }
    }
}