using System;

namespace IMS.Api.ParamModel
{
    public class NewCommodity
    {
        // 店铺Id
        public int FromStoreId { get; set; }
        // 商品图片Id
        public string CommodityImagesId { get; set; }
        // 商品类型Id
        public string CommodityTypeId { get; set; }
        // 店铺名称
        public string StoreName { get; set; }
        // 商品类型名称
        public string CommodityTypeName { get; set; }
        // 商品名称
        public string CommodityName { get; set; }
        // 商品价格
        public double CommodityPrices { get; set; }
        // 商品库存
        public int CommodityStocks { get; set; }
        // 商品详情
        public string CommodityDetails { get; set; }
        // 商品参数Id
        public string CommodityParamsId { get; set; }
        // 商品出售地址
        public string CommodityAddress { get; set; }
    }
}