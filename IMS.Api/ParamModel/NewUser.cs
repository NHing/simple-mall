using System;

namespace IMS.Api.ParamModel
{
    public class NewUser
    {
        // 用户名
        public string Username { get; set; }
        // 密码
        public string Password { get; set; }
        // 昵称
        public string Nickname { get; set; }
        // 头像Id
        public int AvatarId { get; set; }
        // 手机号码
        public string TelephoneNumber { get; set; }
        // 用户收货地址
        public string Address { get; set; }
        // 用户角色Id(0--管理员用户，1--普通用户)
        public int RoleId { get; set; }
        // 邮箱
        public string Email { get; set; }
        // 修改类型
        public int ModType { get; set; }
        // 注册类型
        public int RegisterType { get; set; }
    }
}