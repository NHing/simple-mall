using System;

namespace IMS.Api.ParamModel
{
    public class NewAddress
    {
        // 用户Id
        public int FromUserId { get; set; }
        // 收货人
        public string Consignee { get; set; }
        // 用户地址
        public string UserAddress { get; set; }
        // 用户电话
        public string TelephoneNumber { get; set; }

    }
}