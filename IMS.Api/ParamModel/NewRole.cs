using System;

namespace IMS.Api.ParamModel
{
    public class NewRole
    {
        // 角色名
        public string RoleName { get; set; }
        // 角色数量总和
        public int UsersCount { get; set; }
        // 角色创建人
        public string Creator { get; set; }
        // 角色创建人Id
        public int UserId { get; set; }
        // 角色备注
        public string Remarks { get; set; }
    }
}