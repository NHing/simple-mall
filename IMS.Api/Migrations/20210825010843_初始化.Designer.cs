﻿// <auto-generated />
using System;
using IMS.Api.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace IMS.Api.Migrations
{
    [DbContext(typeof(IMSDb))]
    [Migration("20210825010843_初始化")]
    partial class 初始化
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.9")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("IMS.Api.Entity.Addresses", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Consignee")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("FromUserId")
                        .HasColumnType("integer");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<string>("TelephoneNumber")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("UserAddress")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Addresses");
                });

            modelBuilder.Entity("IMS.Api.Entity.AuditInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("BrowserInfo")
                        .HasColumnType("text");

                    b.Property<string>("ClientIpAddress")
                        .HasColumnType("text");

                    b.Property<string>("ClientName")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("CustomData")
                        .HasColumnType("text");

                    b.Property<string>("Exception")
                        .HasColumnType("text");

                    b.Property<int>("ExecutionDuration")
                        .HasColumnType("integer");

                    b.Property<DateTime>("ExecutionTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("MethodName")
                        .HasColumnType("text");

                    b.Property<string>("Parameters")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<string>("ReturnValue")
                        .HasColumnType("text");

                    b.Property<string>("ServiceName")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("UserInfo")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("AuditInfos");
                });

            modelBuilder.Entity("IMS.Api.Entity.Commodities", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CommodityAddress")
                        .HasColumnType("text");

                    b.Property<string>("CommodityDetails")
                        .HasColumnType("text");

                    b.Property<string>("CommodityImagesId")
                        .HasColumnType("text");

                    b.Property<string>("CommodityName")
                        .HasColumnType("text");

                    b.Property<string>("CommodityParamsId")
                        .HasColumnType("text");

                    b.Property<double>("CommodityPrices")
                        .HasColumnType("double precision");

                    b.Property<int>("CommodityStocks")
                        .HasColumnType("integer");

                    b.Property<string>("CommodityTypeId")
                        .HasColumnType("text");

                    b.Property<string>("CommodityTypeName")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("FromStoreId")
                        .HasColumnType("integer");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<string>("StoreName")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("Commodities");
                });

            modelBuilder.Entity("IMS.Api.Entity.CommodityOrders", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CommodityInfo")
                        .HasColumnType("text");

                    b.Property<string>("Consignee")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("DeliveryAddress")
                        .HasColumnType("text");

                    b.Property<int>("DeliveryAddressId")
                        .HasColumnType("integer");

                    b.Property<bool>("DeliveryStatus")
                        .HasColumnType("boolean");

                    b.Property<int>("FromUserId")
                        .HasColumnType("integer");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("OrderNumber")
                        .HasColumnType("text");

                    b.Property<int>("PaymentAmount")
                        .HasColumnType("integer");

                    b.Property<int>("PaymentId")
                        .HasColumnType("integer");

                    b.Property<string>("PaymentName")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<bool>("SettlementStatus")
                        .HasColumnType("boolean");

                    b.Property<string>("TelephoneNumber")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("UserName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("CommodityOrders");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CommodityInfo = "1,2",
                            Consignee = "大佬大佬",
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(6838),
                            DeliveryAddress = "福建省,莆田市,仙游县 银河系",
                            DeliveryAddressId = 1,
                            DeliveryStatus = false,
                            FromUserId = 1,
                            IsActived = false,
                            IsDeleted = false,
                            OrderNumber = "202108237412",
                            PaymentAmount = 999,
                            PaymentId = 1,
                            PaymentName = "微信支付",
                            SettlementStatus = false,
                            TelephoneNumber = "17679963695",
                            UpdatedTime = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            UserName = "admin1"
                        });
                });

            modelBuilder.Entity("IMS.Api.Entity.CommodityParams", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CommodityParamName")
                        .HasColumnType("text");

                    b.Property<int>("CommodityTypesId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("ParamChildren")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("CommodityParams");
                });

            modelBuilder.Entity("IMS.Api.Entity.CommodityPics", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<int>("PicId")
                        .HasColumnType("integer");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("CommodityPics");
                });

            modelBuilder.Entity("IMS.Api.Entity.CommodityTypes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("CommodityLevel")
                        .HasColumnType("integer");

                    b.Property<int>("CommodityPid")
                        .HasColumnType("integer");

                    b.Property<string>("CommodityTypeName")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("CommodityTypes");
                });

            modelBuilder.Entity("IMS.Api.Entity.Payments", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("PaymentType")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("Payments");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4606),
                            IsActived = true,
                            IsDeleted = false,
                            PaymentType = "微信支付",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4611)
                        },
                        new
                        {
                            Id = 2,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4632),
                            IsActived = true,
                            IsDeleted = false,
                            PaymentType = "支付宝支付",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4633)
                        },
                        new
                        {
                            Id = 3,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4634),
                            IsActived = true,
                            IsDeleted = false,
                            PaymentType = "银行卡支付",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4635)
                        },
                        new
                        {
                            Id = 4,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4637),
                            IsActived = true,
                            IsDeleted = false,
                            PaymentType = "信用卡支付",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4638)
                        });
                });

            modelBuilder.Entity("IMS.Api.Entity.Roles", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Creator")
                        .HasColumnType("text");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<string>("RoleName")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("UsersCount")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Roles");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2834),
                            Creator = "超级管理员",
                            IsActived = true,
                            IsDeleted = false,
                            RoleName = "超级管理员",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2842),
                            UsersCount = 1
                        },
                        new
                        {
                            Id = 2,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2871),
                            Creator = "超级管理员",
                            IsActived = true,
                            IsDeleted = false,
                            RoleName = "供货商",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2873),
                            UsersCount = 1
                        },
                        new
                        {
                            Id = 3,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2878),
                            Creator = "超级管理员",
                            IsActived = true,
                            IsDeleted = false,
                            RoleName = "普通用户",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2879),
                            UsersCount = 1
                        });
                });

            modelBuilder.Entity("IMS.Api.Entity.ShoppingCarts", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CommodityAddress")
                        .HasColumnType("text");

                    b.Property<int>("CommodityId")
                        .HasColumnType("integer");

                    b.Property<string>("CommodityImagesId")
                        .HasColumnType("text");

                    b.Property<string>("CommodityName")
                        .HasColumnType("text");

                    b.Property<int>("CommodityNum")
                        .HasColumnType("integer");

                    b.Property<string>("CommodityParams")
                        .HasColumnType("text");

                    b.Property<double>("CommodityPrice")
                        .HasColumnType("double precision");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("FromUserId")
                        .HasColumnType("integer");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("ShoppingCarts");
                });

            modelBuilder.Entity("IMS.Api.Entity.Slideshows", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("CommodityId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<int>("PicId")
                        .HasColumnType("integer");

                    b.Property<string>("PicName")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("Slideshows");
                });

            modelBuilder.Entity("IMS.Api.Entity.StoreFans", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<byte>("FocusStatus")
                        .HasColumnType("smallint");

                    b.Property<int>("FromStoreId")
                        .HasColumnType("integer");

                    b.Property<int>("FromUserId")
                        .HasColumnType("integer");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("StoreFans");
                });

            modelBuilder.Entity("IMS.Api.Entity.Stores", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<string>("StoreCreator")
                        .HasColumnType("text");

                    b.Property<int>("StoreFans")
                        .HasColumnType("integer");

                    b.Property<int>("StoreLogoId")
                        .HasColumnType("integer");

                    b.Property<string>("StoreName")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("Stores");
                });

            modelBuilder.Entity("IMS.Api.Entity.UploadFileInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ContentType")
                        .HasColumnType("text");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("CurrentName")
                        .HasColumnType("text");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("OriginName")
                        .HasColumnType("text");

                    b.Property<string>("RelativePath")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.ToTable("UploadFileInfos");
                });

            modelBuilder.Entity("IMS.Api.Entity.Users", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Address")
                        .HasColumnType("text");

                    b.Property<int>("AvatarId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Email")
                        .HasColumnType("text");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("Nickname")
                        .HasColumnType("text");

                    b.Property<string>("Password")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<int>("RoleId")
                        .HasColumnType("integer");

                    b.Property<string>("TelephoneNumber")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Username")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AvatarId = 0,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 791, DateTimeKind.Local).AddTicks(9131),
                            Email = "",
                            IsActived = true,
                            IsDeleted = false,
                            Nickname = "超级管理员",
                            Password = "123456",
                            RoleId = 1,
                            TelephoneNumber = "",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2454),
                            Username = "admin1"
                        },
                        new
                        {
                            Id = 2,
                            AvatarId = 0,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2942),
                            Email = "",
                            IsActived = true,
                            IsDeleted = false,
                            Nickname = "超级管理员",
                            Password = "123456",
                            RoleId = 2,
                            TelephoneNumber = "",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2958),
                            Username = "admin2"
                        },
                        new
                        {
                            Id = 3,
                            AvatarId = 0,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2972),
                            Email = "",
                            IsActived = true,
                            IsDeleted = false,
                            Nickname = "超级管理员",
                            Password = "123456",
                            RoleId = 3,
                            TelephoneNumber = "",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2973),
                            Username = "admin3"
                        });
                });

            modelBuilder.Entity("IMS.Api.Entity.WebsiteInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("CreatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("DomainName")
                        .HasColumnType("text");

                    b.Property<string>("ICPNumber")
                        .HasColumnType("text");

                    b.Property<bool>("IsActived")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("boolean");

                    b.Property<string>("PoliceNumber")
                        .HasColumnType("text");

                    b.Property<string>("Remarks")
                        .HasColumnType("text");

                    b.Property<DateTime>("UpdatedTime")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("WebsiteLogoId")
                        .HasColumnType("integer");

                    b.Property<string>("WebsiteName")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("WebsiteInfos");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4053),
                            DomainName = "待定",
                            ICPNumber = "待定",
                            IsActived = true,
                            IsDeleted = false,
                            PoliceNumber = "待定",
                            UpdatedTime = new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4059),
                            WebsiteLogoId = 0,
                            WebsiteName = "待定"
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
