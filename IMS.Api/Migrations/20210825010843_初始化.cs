﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace IMS.Api.Migrations
{
    public partial class 初始化 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FromUserId = table.Column<int>(type: "integer", nullable: false),
                    Consignee = table.Column<string>(type: "text", nullable: true),
                    UserAddress = table.Column<string>(type: "text", nullable: true),
                    TelephoneNumber = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AuditInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Parameters = table.Column<string>(type: "text", nullable: true),
                    BrowserInfo = table.Column<string>(type: "text", nullable: true),
                    ClientName = table.Column<string>(type: "text", nullable: true),
                    ClientIpAddress = table.Column<string>(type: "text", nullable: true),
                    ExecutionDuration = table.Column<int>(type: "integer", nullable: false),
                    ExecutionTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ReturnValue = table.Column<string>(type: "text", nullable: true),
                    Exception = table.Column<string>(type: "text", nullable: true),
                    MethodName = table.Column<string>(type: "text", nullable: true),
                    ServiceName = table.Column<string>(type: "text", nullable: true),
                    UserInfo = table.Column<string>(type: "text", nullable: true),
                    CustomData = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Commodities",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FromStoreId = table.Column<int>(type: "integer", nullable: false),
                    CommodityImagesId = table.Column<string>(type: "text", nullable: true),
                    CommodityTypeId = table.Column<string>(type: "text", nullable: true),
                    StoreName = table.Column<string>(type: "text", nullable: true),
                    CommodityTypeName = table.Column<string>(type: "text", nullable: true),
                    CommodityName = table.Column<string>(type: "text", nullable: true),
                    CommodityPrices = table.Column<double>(type: "double precision", nullable: false),
                    CommodityStocks = table.Column<int>(type: "integer", nullable: false),
                    CommodityDetails = table.Column<string>(type: "text", nullable: true),
                    CommodityParamsId = table.Column<string>(type: "text", nullable: true),
                    CommodityAddress = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commodities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommodityOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FromUserId = table.Column<int>(type: "integer", nullable: false),
                    UserName = table.Column<string>(type: "text", nullable: true),
                    CommodityInfo = table.Column<string>(type: "text", nullable: true),
                    DeliveryAddressId = table.Column<int>(type: "integer", nullable: false),
                    TelephoneNumber = table.Column<string>(type: "text", nullable: true),
                    Consignee = table.Column<string>(type: "text", nullable: true),
                    DeliveryAddress = table.Column<string>(type: "text", nullable: true),
                    PaymentAmount = table.Column<int>(type: "integer", nullable: false),
                    PaymentId = table.Column<int>(type: "integer", nullable: false),
                    PaymentName = table.Column<string>(type: "text", nullable: true),
                    OrderNumber = table.Column<string>(type: "text", nullable: true),
                    SettlementStatus = table.Column<bool>(type: "boolean", nullable: false),
                    DeliveryStatus = table.Column<bool>(type: "boolean", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityOrders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommodityParams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CommodityParamName = table.Column<string>(type: "text", nullable: true),
                    CommodityTypesId = table.Column<int>(type: "integer", nullable: false),
                    ParamChildren = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityParams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommodityPics",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PicId = table.Column<int>(type: "integer", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityPics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommodityTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CommodityTypeName = table.Column<string>(type: "text", nullable: true),
                    CommodityPid = table.Column<int>(type: "integer", nullable: false),
                    CommodityLevel = table.Column<int>(type: "integer", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommodityTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PaymentType = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleName = table.Column<string>(type: "text", nullable: true),
                    UsersCount = table.Column<int>(type: "integer", nullable: false),
                    Creator = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShoppingCarts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FromUserId = table.Column<int>(type: "integer", nullable: false),
                    CommodityId = table.Column<int>(type: "integer", nullable: false),
                    CommodityName = table.Column<string>(type: "text", nullable: true),
                    CommodityPrice = table.Column<double>(type: "double precision", nullable: false),
                    CommodityNum = table.Column<int>(type: "integer", nullable: false),
                    CommodityParams = table.Column<string>(type: "text", nullable: true),
                    CommodityImagesId = table.Column<string>(type: "text", nullable: true),
                    CommodityAddress = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingCarts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Slideshows",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PicId = table.Column<int>(type: "integer", nullable: false),
                    PicName = table.Column<string>(type: "text", nullable: true),
                    CommodityId = table.Column<int>(type: "integer", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Slideshows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StoreFans",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FromStoreId = table.Column<int>(type: "integer", nullable: false),
                    FromUserId = table.Column<int>(type: "integer", nullable: false),
                    FocusStatus = table.Column<byte>(type: "smallint", nullable: false),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreFans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stores",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StoreLogoId = table.Column<int>(type: "integer", nullable: false),
                    StoreName = table.Column<string>(type: "text", nullable: true),
                    StoreFans = table.Column<int>(type: "integer", nullable: false),
                    StoreCreator = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UploadFileInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OriginName = table.Column<string>(type: "text", nullable: true),
                    CurrentName = table.Column<string>(type: "text", nullable: true),
                    RelativePath = table.Column<string>(type: "text", nullable: true),
                    ContentType = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadFileInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Username = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true),
                    Nickname = table.Column<string>(type: "text", nullable: true),
                    AvatarId = table.Column<int>(type: "integer", nullable: false),
                    TelephoneNumber = table.Column<string>(type: "text", nullable: true),
                    Address = table.Column<string>(type: "text", nullable: true),
                    RoleId = table.Column<int>(type: "integer", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebsiteInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WebsiteLogoId = table.Column<int>(type: "integer", nullable: false),
                    WebsiteName = table.Column<string>(type: "text", nullable: true),
                    DomainName = table.Column<string>(type: "text", nullable: true),
                    ICPNumber = table.Column<string>(type: "text", nullable: true),
                    PoliceNumber = table.Column<string>(type: "text", nullable: true),
                    IsActived = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Remarks = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebsiteInfos", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "CommodityOrders",
                columns: new[] { "Id", "CommodityInfo", "Consignee", "CreatedTime", "DeliveryAddress", "DeliveryAddressId", "DeliveryStatus", "FromUserId", "IsActived", "IsDeleted", "OrderNumber", "PaymentAmount", "PaymentId", "PaymentName", "Remarks", "SettlementStatus", "TelephoneNumber", "UpdatedTime", "UserName" },
                values: new object[] { 1, "1,2", "大佬大佬", new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(6838), "福建省,莆田市,仙游县 银河系", 1, false, 1, false, false, "202108237412", 999, 1, "微信支付", null, false, "17679963695", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin1" });

            migrationBuilder.InsertData(
                table: "Payments",
                columns: new[] { "Id", "CreatedTime", "IsActived", "IsDeleted", "PaymentType", "Remarks", "UpdatedTime" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4606), true, false, "微信支付", null, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4611) },
                    { 2, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4632), true, false, "支付宝支付", null, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4633) },
                    { 3, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4634), true, false, "银行卡支付", null, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4635) },
                    { 4, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4637), true, false, "信用卡支付", null, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4638) }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "CreatedTime", "Creator", "IsActived", "IsDeleted", "Remarks", "RoleName", "UpdatedTime", "UsersCount" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2834), "超级管理员", true, false, null, "超级管理员", new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2842), 1 },
                    { 2, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2871), "超级管理员", true, false, null, "供货商", new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2873), 1 },
                    { 3, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2878), "超级管理员", true, false, null, "普通用户", new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(2879), 1 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Address", "AvatarId", "CreatedTime", "Email", "IsActived", "IsDeleted", "Nickname", "Password", "Remarks", "RoleId", "TelephoneNumber", "UpdatedTime", "Username" },
                values: new object[,]
                {
                    { 1, null, 0, new DateTime(2021, 8, 25, 9, 8, 42, 791, DateTimeKind.Local).AddTicks(9131), "", true, false, "超级管理员", "123456", null, 1, "", new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2454), "admin1" },
                    { 2, null, 0, new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2942), "", true, false, "超级管理员", "123456", null, 2, "", new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2958), "admin2" },
                    { 3, null, 0, new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2972), "", true, false, "超级管理员", "123456", null, 3, "", new DateTime(2021, 8, 25, 9, 8, 42, 793, DateTimeKind.Local).AddTicks(2973), "admin3" }
                });

            migrationBuilder.InsertData(
                table: "WebsiteInfos",
                columns: new[] { "Id", "CreatedTime", "DomainName", "ICPNumber", "IsActived", "IsDeleted", "PoliceNumber", "Remarks", "UpdatedTime", "WebsiteLogoId", "WebsiteName" },
                values: new object[] { 1, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4053), "待定", "待定", true, false, "待定", null, new DateTime(2021, 8, 25, 9, 8, 42, 794, DateTimeKind.Local).AddTicks(4059), 0, "待定" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "AuditInfos");

            migrationBuilder.DropTable(
                name: "Commodities");

            migrationBuilder.DropTable(
                name: "CommodityOrders");

            migrationBuilder.DropTable(
                name: "CommodityParams");

            migrationBuilder.DropTable(
                name: "CommodityPics");

            migrationBuilder.DropTable(
                name: "CommodityTypes");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "ShoppingCarts");

            migrationBuilder.DropTable(
                name: "Slideshows");

            migrationBuilder.DropTable(
                name: "StoreFans");

            migrationBuilder.DropTable(
                name: "Stores");

            migrationBuilder.DropTable(
                name: "UploadFileInfos");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "WebsiteInfos");
        }
    }
}
