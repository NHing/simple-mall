using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using IMS.Api.Entity;
using IMS.Api.Db;

namespace IMS.Api.Repository
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// 数据库上下文的变量，此处是使用常规手段，直接初始化一个数据库上下文的实例对象
        /// </summary>
        /// <returns></returns>
        // private IMSDb _db = new IMSDb();
        private IMSDb _db;

        public EFRepository(IMSDb db)
        {
            _db = db;
        }


        /// <summary>
        /// 一个字段成员，用于内部Entity属性
        /// </summary>
        private DbSet<T> _entity;

        /// <summary>
        /// 一个访问受限的属性，只有访问器，总是返回一个代表T类型的表对象
        /// </summary>
        /// <value></value>
        protected DbSet<T> Entity
        {
            get
            {
                if (_entity == null)
                {
                    _entity = _db.Set<T>();
                }
                return _entity;
            }
        }


        /// <summary>
        /// 代表一个可以用于查询T类型的表
        /// </summary>
        /// <value></value>
        public IQueryable<T> Table
        {
            get
            {
                // 只查询存在的数据
                return Entity.AsQueryable<T>().Where(x=>x.IsDeleted!=true);
            }
        }

        public IQueryable<T> DeleteTable
        {
            get
            {
                // 只查询删除的数据
                return Entity.AsQueryable<T>().Where(x=>x.IsDeleted==true);
            }
        }
        /// <summary>
        /// 根据Id改变激活状态
        /// </summary>
        /// <param name="id"></param>
        public void ActiveStatus(int id)
        {
            var t = Entity.Where(x => x.Id == id).FirstOrDefault();
            t.IsActived=!t.IsActived;
            t.UpdatedTime=DateTime.Now;
            Entity.Update(t);
            _db.SaveChanges();
        }

        /// <summary>
        /// 根据Id改变恢复已被删除的数据
        /// </summary>
        /// <param name="id"></param>
        public void Recovery(int id)
        {
            var t = Entity.Where(x => x.Id == id).FirstOrDefault();
            t.IsActived=true;
            t.IsDeleted=false;
            t.UpdatedTime=DateTime.Now;
            Entity.Update(t);
            _db.SaveChanges();
        }
        
        /// <summary>
        /// 根据Id改变删除状态（伪删除）
        /// </summary>
        /// <param name="id"></param>
        public void PseudoDeletion(int id)
        {
            var t = Entity.Where(x => x.Id == id).FirstOrDefault();
            t.IsActived=false;
            t.IsDeleted=true;
            t.UpdatedTime=DateTime.Now;
            Entity.Update(t);
            _db.SaveChanges();
        }
        /// <summary>
        /// 删除(指定T类型，在数据库当中代表指定的表)指定Id的记录
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var t = Entity.Where(x => x.Id == id).FirstOrDefault();
            Entity.Remove(t);
            _db.SaveChanges();
        }

        public T GetById(int id)
        {
            var t = Entity.Where(x => x.Id == id).FirstOrDefault();
            return t;
        }


        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }

            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;

            Entity.Add(entity);
            _db.SaveChanges();
        }

        public void InsertBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
            }


            Entity.AddRange(entities);
            _db.SaveChanges();
        }

        public async Task InsertAsync(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }

            entity.IsActived = true;
            entity.IsDeleted = false;
            entity.CreatedTime = DateTime.Now;
            entity.UpdatedTime = DateTime.Now;

            await Entity.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task InsertBulkAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsActived = true;
                entity.IsDeleted = false;
                entity.CreatedTime = DateTime.Now;
                entity.UpdatedTime = DateTime.Now;
            }



            await Entity.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }


        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new NotImplementedException();
            }

            entity.UpdatedTime = DateTime.Now;
            Entity.Update(entity);
            _db.SaveChanges();
        }

        public void UpdateBulk(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                entity.UpdatedTime = DateTime.Now;
            }

            _db.UpdateRange(entities);
            _db.SaveChanges();
        }
    }
}