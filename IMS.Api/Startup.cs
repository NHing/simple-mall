using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuditLogDemo.Fliters;
using IMS.Api.Repository;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace IMS.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // 跨域定义字段
        private string allowCors = "AllowCors";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // 跨域
            services.AddCors(Options =>
            {
                Options.AddPolicy(allowCors, builder =>
                {
                    builder.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
                });
            });
            // 注册数据库上下文到容器
            services.AddDbContext<IMS.Api.Db.IMSDb>(o => o.UseNpgsql(Configuration.GetConnectionString("PostgreSQL")));
            // 注册对数据库的基本操作服务到容器
            services.AddScoped(typeof(IRepository<>), typeof(EFRepository<>));
            // 注册验证器（使用何种配置来验证token）
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(option =>
            {
                // 是否要求使用https
                option.RequireHttpsMetadata = false;
                // 是否保存token
                option.SaveToken = true;
                // 使用配置中间件，获得token的配置
                var tokenParameter = Configuration.GetSection("TokenParameter").Get<TokenParameter>();
                // 验证器的核心属性
                option.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,//要不要验证生成token的密钥
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer = true, // 要不要验证发行token的人（个人或者组织）
                    ValidIssuer = tokenParameter.Issuer,
                    ValidateAudience = false, // 是否验证受众
                    ValidateLifetime = true, // 验证token的有效期
                    ClockSkew = TimeSpan.Zero
                };
                option.Events = new JwtBearerEvents
                {
                    //token验证失败后执行
                    OnChallenge = context =>
                    {
                        // 跳过默认响应逻辑
                        context.HandleResponse();
                        // 自定义401时返回的信息
                        var result = JsonHelper.Serialize(new { Code = "401", Message = "验证失败" });
                        context.Response.ContentType = "application/json";
                        //验证失败返回401
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        context.Response.WriteAsync(result);
                        return Task.FromResult(0);
                    }
                };
            });
            services.AddControllers(options =>
            {
                // 日志审计
                options.Filters.Add(typeof(AuditLogActionFilter));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // 由于请求通过反向代理转发，因此使用 Microsoft.AspNetCore.HttpOverrides 包中的转接头中间件。
            // 此中间件使用 X-Forwarded-Proto 标头来更新 Request.Scheme，使重定向 URI 和其他安全策略能够正常工作。
            // 简单来说，使用nginx的反向代理，需要使用这个中间件（好吧，通过实际测试，没有的情况下，token是没有问题的，其它情况未测试）
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseRouting();

            // 将token的验证注册到中间件
            app.UseAuthentication();
            app.UseAuthorization();



            // 跨域
            app.UseCors(allowCors);
            // // 提供静态文件中间件
            // app.UseStaticFiles(new StaticFileOptions
            // {
            //     FileProvider = new PhysicalFileProvider(
            //         Path.Combine(Directory.GetCurrentDirectory(), "UploadFiles")),
            //     RequestPath = "/UploadFiles"
            // });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
