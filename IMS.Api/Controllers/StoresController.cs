﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class StoresController : ControllerBase
    {
        // 定义调用店铺表的接口
        private IRepository<Stores> _storesRepository;
        // 定义调用用户表的接口
        private IRepository<Users> _usersRepository;
        // 定义调用店铺类型表的接口
        private IRepository<CommodityTypes> _commodityTypesRepository;
        // 定义商铺表的接口
        private IRepository<Stores> _storesTypesRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public StoresController(IConfiguration configuration, IRepository<Stores> storesRepository, IRepository<Users> usersRepository, IRepository<CommodityTypes> storeTypesRepository, IRepository<Stores> storesTypesRepository)
        {
            _configuration = configuration;
            _usersRepository = usersRepository;
            _storesRepository = storesRepository;
            _commodityTypesRepository = storeTypesRepository;
            _storesTypesRepository = storesTypesRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 跳过token验证
        [AllowAnonymous]
        // 获取店铺表数据
        [HttpGet]
        public dynamic GetStoresList([FromQuery] Pager pager)
        {
            // 页码（当前第几页）
            var pageIndex = pager.PageIndex;
            // 页大小（一页里面行的数量）
            var pageSize = pager.PageSize;
            // 查询的内容
            var query = pager.Query;
            // 查询的类型
            var queryType = pager.QueryType;
            // 判断是否查询
            if (string.IsNullOrEmpty(query))
            {
                var stores = _storesRepository.Table.ToList();
                // 判断店铺表是否为空
                if (stores != null)
                {
                    // 店铺表不为空
                    // 判断是否进入回收站
                    var recycle = pager.Recycle;
                    if (recycle)
                    {
                        // 进入回收站
                        // 查看状态为删除店铺表
                        var deletedStore = _storesRepository.DeleteTable;
                        // 给状态为删除的店铺表分页
                        var deletedTable = deletedStore.OrderByDescending(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

                        return JsonHelper.Serialize(new
                        {
                            Data = new { DeletedStores = deletedTable, Pager = new { pageIndex, pageSize, PageTotal = deletedStore.Count() } },
                            Meta = new
                            {
                                Msg = "获取已删除店铺列表成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 没有进入回收站
                        // 类型表
                        var storeTypes = _commodityTypesRepository.Table.ToList();
                        // 店铺表分页
                        var activedTable = stores.OrderByDescending(x => x.Id).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

                        return JsonHelper.Serialize(new
                        {
                            Data = new { Stores = activedTable, CommodityTypes = storeTypes, Pager = new { pageIndex, pageSize, PageTotal = stores.Count() } },
                            Meta = new
                            {
                                Msg = "获取店铺列表成功！",
                                Status = 200
                            }
                        });
                    }
                }
                else
                {
                    // 店铺表为空
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "店铺列表获取异常！",
                            Status = 400
                        }
                    });
                }

            }
            else
            {
                // 判断查找类型（0--根据店铺名称查找，1--创始人查询）
                if (queryType == 0)
                {
                    // 根据店铺名称查找
                    var stores = _storesRepository.Table.ToList().Where(x => x.StoreName.Contains(query));
                    // 判断查询的店铺名称是否为空
                    if (stores != null)
                    {
                        // 查询的店铺名称不为空
                        // 给查询到的内容分页
                        var activedTable = stores.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                        return JsonHelper.Serialize(new
                        {
                            Data = new { Stores = activedTable, Pager = new { pageIndex, pageSize, PageTotal = stores.Count() } },
                            Meta = new
                            {
                                Msg = "店铺数据查询成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 查询的店铺名称为空
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = "没有找到你查询的店铺名称！",
                                Status = 400
                            }
                        });
                    }
                }
                else if (queryType == 1)
                {
                    // 根据店铺创始人查询
                    var stores = _storesRepository.Table.ToList().Where(x => x.StoreCreator.Contains(query));
                    // 判断查询的创始人是否为空
                    if (stores != null)
                    {
                        // 查询店铺创始人不为空
                        // 给查询到的内容分页
                        var activedTable = stores.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                        return JsonHelper.Serialize(new
                        {
                            Data = new { Stores = activedTable, Pager = new { pageIndex, pageSize, PageTotal = stores.Count() } },
                            Meta = new
                            {
                                Msg = "店铺创始人数据查询成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 查询的创始人为空
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = "没有找到你查询的创始人！",
                                Status = 400
                            }
                        });
                    }
                }
                else
                {
                    // 查询的类型有误
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "你的查询请求类型有误！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // // 跳过token验证
        // [AllowAnonymous]
        // // 根据排行榜请求获取店铺
        // [HttpGet("ranking/{id}"), Route("ranking")]
        // public dynamic GetStore(int id)
        // {
        //     // 排行榜获取列表请求类型（1--阅读量排行2--点赞排行3--评论排行）
        //     if (id == 1)
        //     {
        //         // 阅读量排行
        //         var storesViewsOrder = _storesRepository.Table.ToList().OrderByDescending(x => x.StoreViews).Take(10);
        //         if (storesViewsOrder != null)
        //         {
        //             // 阅读量排行获取成功
        //             return JsonHelper.Serialize(new
        //             {
        //                 Data = storesViewsOrder,
        //                 Meta = new
        //                 {
        //                     Msg = "阅读量排行榜数据获取成功！",
        //                     Status = 200
        //                 }
        //             });
        //         }
        //         else
        //         {
        //             // 阅读量排行获取失败
        //             return JsonHelper.Serialize(new
        //             {
        //                 Data = "",
        //                 Meta = new
        //                 {
        //                     Msg = "阅读量排行榜数据获取异常！",
        //                     Status = 400
        //                 }
        //             });
        //         }
        //     }
        //     else if (id == 2)
        //     {
        //         // 点赞排行
        //         var articlePraiseCountOrder = _storesRepository.Table.ToList().OrderByDescending(x => x.StorePraiseCount).Take(10);
        //         if (articlePraiseCountOrder != null)
        //         {
        //             // 点赞排行获取成功
        //             return JsonHelper.Serialize(new
        //             {
        //                 Data = articlePraiseCountOrder,
        //                 Meta = new
        //                 {
        //                     Msg = "点赞排行榜数据获取成功！",
        //                     Status = 200
        //                 }
        //             });
        //         }
        //         else
        //         {
        //             // 点赞排行获取失败
        //             return JsonHelper.Serialize(new
        //             {
        //                 Data = "",
        //                 Meta = new
        //                 {
        //                     Msg = "点赞排行榜数据获取异常！",
        //                     Status = 400
        //                 }
        //             });
        //         }
        //     }
        //     else if (id == 3)
        //     {
        //         // 评论排行
        //         var articleCommentCountOrder = _storesRepository.Table.ToList().OrderByDescending(x => x.StoreCommentCount).Take(10);
        //         if (articleCommentCountOrder != null)
        //         {
        //             // 评论排行获取成功
        //             return JsonHelper.Serialize(new
        //             {
        //                 Data = articleCommentCountOrder,
        //                 Meta = new
        //                 {
        //                     Msg = "评论排行榜数据获取成功！",
        //                     Status = 200
        //                 }
        //             });
        //         }
        //         else
        //         {
        //             // 评论排行获取失败
        //             return JsonHelper.Serialize(new
        //             {
        //                 Data = "",
        //                 Meta = new
        //                 {
        //                     Msg = "评论排行榜数据获取异常！",
        //                     Status = 400
        //                 }
        //             });
        //         }
        //     }
        //     else
        //     {
        //         // 请求查询排行榜类型有误
        //         return JsonHelper.Serialize(new
        //         {
        //             Data = "",
        //             Meta = new
        //             {
        //                 Msg = "排行榜数据获取异常，请求类型有误！",
        //                 Status = 400
        //             }
        //         });
        //     }
        // }

        // 跳过token验证
        [AllowAnonymous]
        // 根据店铺Id获取店铺
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetStoreById(int id)
        {
            var stores = _storesRepository.GetById(id);
            // var categoryId = _commodityTypesRepository.Table.Where(x => x.Id == stores.CategoryId).ToList()[0].Id;
            // 判断传入的id是否在类型表存在
            if (stores != null)
            {
                // 查询的Id的店铺表存在
                return JsonHelper.Serialize(new
                {
                    Data = new { Stores = stores },
                    Meta = new
                    {
                        Msg = "店铺数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的店铺表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "店铺数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 添加新店铺
        [HttpPost]
        public dynamic AddStore(NewStore model)
        {
            // 店铺LogoId
            var storeLogoId = model.StoreLogoId;
            // 店铺名称
            var storeName = model.StoreName;
            // 店铺粉丝数总和
            var StoreFans = 0;
            // 商店创始人
            var storeCreator = _usersRepository.Table.ToList().Where(x => x.Id == model.CreatedUserId).FirstOrDefault().Nickname;

            // 判断店铺标题和店铺内容是否为空
            if (string.IsNullOrEmpty(storeName) && string.IsNullOrEmpty(storeCreator))
            {
                // 店铺名称和店铺创始人为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入的店铺名称和店铺创始人不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 店铺标题和店铺内容不为空
                var stores = new Stores
                {
                    StoreLogoId = storeLogoId,
                    StoreName = storeName,
                    StoreFans = StoreFans,
                    StoreCreator = storeCreator
                };
                // 插入店铺
                _storesRepository.Insert(stores);

                // // 查询对应类型Id对应的类型数据
                // var storeTypes = _commodityTypesRepository.GetById(categoryId);
                // // 更新一下类型表对应类型总数
                // var storesCount = _storesRepository.Table.ToList().Where(x => x.CategoryId == categoryId).Count();
                // // 类型店铺总数
                // storeTypes.StoresCount = storesCount;
                // // 更新类型表店铺总数
                // _commodityTypesRepository.Update(storeTypes);

                return JsonHelper.Serialize(new
                {
                    Data = stores,
                    Meta = new
                    {
                        Msg = "创建成功！",
                        Status = 200
                    }
                });
            }
        }

        // 店铺信息修改
        [HttpPut("{id}")]
        public dynamic ModStore(int id, NewStore model)
        {
            var stores = _storesRepository.GetById(id);
            // 判断所修改店铺Id是否存在
            if (stores != null)
            {
                // var oldCategoryId = stores.CategoryId;
                // 要修改的店铺Id存在
                // 店铺LogoId
                stores.StoreLogoId = model.StoreLogoId;
                // 店铺名称
                stores.StoreName = model.StoreName;

                // 更新店铺
                _storesRepository.Update(stores);

                // // 查询对应类型Id对应的类型数据
                // var oldStoreTypes = _commodityTypesRepository.GetById(oldCategoryId);
                // // 更新一下类型表对应类型总数
                // var oldStoresCount = _storesRepository.Table.ToList().Where(x => x.CategoryId == oldCategoryId).Count();
                // // 类型店铺总数
                // oldStoreTypes.StoresCount = oldStoresCount;
                // // 更新类型表店铺总数
                // _commodityTypesRepository.Update(oldStoreTypes);

                // // 查询对应类型Id对应的类型数据
                // var storeTypes = _commodityTypesRepository.GetById(model.CategoryId);
                // // 更新一下类型表对应类型总数
                // var storesCount = _storesRepository.Table.ToList().Where(x => x.CategoryId == model.CategoryId).Count();
                // // 类型店铺总数
                // storeTypes.StoresCount = storesCount;
                // // 更新类型表店铺总数
                // _commodityTypesRepository.Update(storeTypes);

                return JsonHelper.Serialize(new
                {
                    Data = stores,
                    Meta = new
                    {
                        Msg = string.Format("你修改的店铺的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 要修改的店铺Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的店铺不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 店铺状态可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var stores = _storesRepository.GetById(id);
            // 判断对应Id的店铺是否存在
            if (stores != null)
            {
                // 指定Id的店铺存在
                _storesRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = stores,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应店铺的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的店铺不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的店铺不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 店铺信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeleteStore(int id)
        {
            var stores = _storesRepository.GetById(id);
            // 判断对应Id的店铺是否存在
            if (stores != null)
            {
                // 对应Id的店铺存在
                // 伪删除操作
                _storesRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = stores,
                    Meta = new
                    {
                        Msg = string.Format("你删除的店铺Id为 {0} 的数据,删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的店铺不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的店铺Id为 {0} 的数据,执行删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 店铺信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteStore(int id)
        {
            var stores = _storesRepository.GetById(id);
            // 判断对应Id的店铺是否存在
            if (stores != null)
            {
                // 指定Id的店铺存在
                // 彻底删除
                _storesRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = stores,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的类型不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的店铺数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryStore(int id)
        {
            var stores = _storesRepository.GetById(id);
            // 判断对应Id的店铺是否存在
            if (stores != null)
            {
                // 指定Id的店铺存在
                // 恢复数据
                _storesRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Data = stores,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的店铺的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 指定Id的店铺不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的店铺不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
