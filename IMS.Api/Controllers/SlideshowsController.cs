﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class SlideshowsController : ControllerBase
    {
        // 定义调用轮播图表的接口
        private IRepository<Slideshows> _slideshowsRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public SlideshowsController(IConfiguration configuration, IRepository<Slideshows> slideshowsRepository)
        {
            _configuration = configuration;
            _slideshowsRepository = slideshowsRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 跳过token认证
        [AllowAnonymous]
        // 轮播图信息数据
        [HttpGet]
        public dynamic GetSlideshowList()
        {
            var slideshow = _slideshowsRepository.Table.ToList();
            // 判断轮播图信息是否为空
            if (slideshow != null)
            {
                // 轮播图信息不为空
                return JsonHelper.Serialize(new
                {
                    Data = new { Slideshows = slideshow },
                    Meta = new
                    {
                        Msg = "获取轮播图信息成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 轮播图信息为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "轮播图信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据Id获取轮播图信息
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetSlideshowById(int id)
        {
            var slideshow = _slideshowsRepository.GetById(id);
            // 判断传入的Id是否在轮播图表中存在
            if (slideshow != null)
            {
                // 传入的Id在轮播图表中存在
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = "轮播图指定Id信息获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的Id在轮播图表中不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "轮播图指定Id信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据PicId获取轮播图信息
        [HttpGet("picid/{picid}"), Route("picid")]
        public dynamic GetSlideshowByPicId(int picid)
        {
            var slideshow = _slideshowsRepository.Table.Where(x => x.PicId == picid).FirstOrDefault();
            // 判断传入的PicId是否在轮播图表中存在
            if (slideshow != null)
            {
                // 传入的PicId在轮播图表中存在
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = "轮播图指定PicId信息获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的PicId在轮播图表中不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "轮播图指定PicId信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 跳过token验证(初始化数据库后可短暂取消忽略，创建初始号后在进行忽略)
        // [AllowAnonymous]
        // 添加轮播图图片
        [HttpPost]
        public dynamic AddSlideshow(NewPicture model)
        {
            // 判断插入轮播图图片Id是否异常
            if (model.PicId < 0)
            {
                // 插入的图片id<0
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入的轮播图信息不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 插入的图片id>0
                var slideshow = new Slideshows
                {
                    PicId = model.PicId,
                    PicName = model.PicName,
                    CommodityId = model.CommodityId
                };
                // 插入数据
                _slideshowsRepository.Insert(slideshow);
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = "添加轮播图图片成功！",
                        Status = 200
                    }
                });
            }
        }

        // 轮播图信息修改
        [HttpPut("{id}")]
        public dynamic ModSlideshow(int id, NewPicture model)
        {
            var slideshow = _slideshowsRepository.GetById(id);
            // 判断所修改轮播图Id是否存在
            if (slideshow != null)
            {
                // 修改轮播图对应的Id存在
                // 轮播图图片Id
                slideshow.PicId = model.PicId;
                // 轮播图图片名称
                slideshow.PicName = model.PicName;
                // 轮播图对应商品Id
                slideshow.CommodityId = model.CommodityId;

                // 更新数据
                _slideshowsRepository.Update(slideshow);
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = string.Format("你修改的轮播图信息对应的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 修改轮播图对应的Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的轮播图信息不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 轮播图信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic Deleteslideshows(int id)
        {
            var slideshow = _slideshowsRepository.GetById(id);
            // 判断对应Id的轮播图是否存在
            if (slideshow != null)
            {
                // 对应Id的轮播图存在
                // 伪删除操作
                _slideshowsRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = string.Format("你删除的轮播图信息Id为 {0} 的数据,删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的轮播图不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的轮播图信息Id为 {0} 的数据,执行删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 轮播图可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var slideshow = _slideshowsRepository.GetById(id);
            // 判断对应Id的轮播图是否存在
            if (slideshow != null)
            {
                // 指定Id的轮播图存在
                _slideshowsRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应轮播图的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的轮播图不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的轮播图不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 轮播图信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteslideshows(int id)
        {
            var slideshow = _slideshowsRepository.GetById(id);
            // 判断对应Id的轮播图是否存在
            if (slideshow != null)
            {
                // 对应Id的轮播图存在
                // 彻底删除
                _slideshowsRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的轮播图信息数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的轮播图不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的轮播图信息数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的轮播图信息数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryWebsiteInfo(int id)
        {
            var slideshow = _slideshowsRepository.GetById(id);
            // 判断对应Id的轮播图是否存在
            if (slideshow != null)
            {
                // 对应Id的轮播图存在
                // 恢复数据
                _slideshowsRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Code = 1000,
                    Data = slideshow,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的轮播图信息的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 对应Id的轮播图不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的轮播图信息不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
