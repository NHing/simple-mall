﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class PaymentsController : ControllerBase
    {
        // 定义调用支付表的接口
        private IRepository<Payments> _paymentsRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public PaymentsController(IConfiguration configuration, IRepository<Payments> paymentsRepository)
        {
            _configuration = configuration;
            _paymentsRepository = paymentsRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 跳过token认证
        [AllowAnonymous]
        // 支付信息数据
        [HttpGet]
        public dynamic GetPaymentList()
        {
            var payment = _paymentsRepository.Table.ToList().OrderBy(x => x.Id);
            // 判断支付信息是否为空
            if (payment != null)
            {
                // 支付信息不为空
                return JsonHelper.Serialize(new
                {
                    Data = new { Payments = payment },
                    Meta = new
                    {
                        Msg = "获取支付信息成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 支付信息为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "支付信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据Id获取支付信息
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetPaymentById(int id)
        {
            var payment = _paymentsRepository.GetById(id);
            // 判断传入的Id是否在支付表中存在
            if (payment != null)
            {
                // 传入的Id在支付表中存在
                return JsonHelper.Serialize(new
                {
                    Data = payment,
                    Meta = new
                    {
                        Msg = "支付指定Id信息获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的Id在支付表中不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "支付指定Id信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 支付状态可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var payment = _paymentsRepository.GetById(id);
            // 判断对应Id的支付类型是否存在
            if (payment != null)
            {
                // 指定Id的支付类型存在
                _paymentsRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = payment,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应支付类型的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的支付类型不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的支付类型不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
