﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        // 定义调用用户表的接口
        private IRepository<Users> _usersRepository;
        // 定义调用用户角色表的接口
        private IRepository<Roles> _rolesRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 
        // 依赖注入
        public UsersController(IConfiguration configuration, IRepository<Users> usersRepository, IRepository<Roles> rolesRepository)
        {
            _configuration = configuration;
            _usersRepository = usersRepository;
            _rolesRepository = rolesRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 获取用户表数据
        [HttpGet]
        public dynamic GetUserList([FromQuery] Pager pager, [FromQuery] int RoleId)
        {
            // 页码（当前第几页）
            var pageIndex = pager.PageIndex;
            // 页码（当前第几页）
            var pageSize = pager.PageSize;
            // 查询的内容
            var query = pager.Query;
            var role = _rolesRepository.Table.ToList().Where(x => x.Id == RoleId).FirstOrDefault();
            // 判断角色id对应的角色是否存在
            if (role != null)
            {
                // 查询角色比表对应Id为空
                // 判断是否查询
                if (string.IsNullOrEmpty(query))
                {

                    var users = _usersRepository.Table.ToList().Where(x => x.RoleId == RoleId);
                    if (users != null)
                    {
                        // 用户表不为空
                        var recycle = pager.Recycle;
                        // 当你点击回收时查询你状态为删除的表
                        if (recycle)
                        {
                            // 查看状态为删除用户表
                            var deletedUser = _usersRepository.DeleteTable.Where(x => x.RoleId == RoleId);
                            // 给状态为删除的用户表分页
                            var deletedTable = deletedUser.Where(x => x.RoleId == RoleId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                            return JsonHelper.Serialize(new
                            {
                                Data = new { DeletedUsers = deletedTable, Pager = new { pageIndex, pageSize, PageTotal = deletedUser.Count() } },
                                Meta = new
                                {
                                    Msg = "获取已删除用户列表成功！",
                                    Status = 200
                                }
                            });
                        }
                        else
                        {
                            // 角色列表
                            var roles = _rolesRepository.Table.ToList();
                            // 给状态为存在的用户表分页
                            var activedTable = users.Where(x => x.RoleId == RoleId).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                            return JsonHelper.Serialize(new
                            {
                                Data = new { Users = activedTable, Roles = roles, Pager = new { pageIndex, pageSize, PageTotal = users.Count() } },
                                Meta = new
                                {
                                    Msg = "获取用户列表成功！",
                                    Status = 200
                                }
                            });
                        }
                    }
                    else
                    {
                        // 用户表为空
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = "用户列表获取异常！",
                                Status = 400
                            }
                        });
                    }

                }
                else
                {
                    // 查询内容不为空
                    // 根据用户名称查找
                    var users = _usersRepository.Table.ToList().Where(x => x.Username.Contains(query) && x.RoleId == RoleId);
                    // 判断查询的用户名对应内容是否为空   
                    if (users != null)
                    {
                        // 查询的用户名对应内容不为空
                        // 给查询到的内容分页
                        var activedTable = users.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                        return JsonHelper.Serialize(new
                        {
                            Data = new { Users = activedTable, Pager = new { pageIndex, pageSize, PageTotal = users.Count() } },
                            Meta = new
                            {
                                Msg = "用户数据查询成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 查询的用户名对应内容为空
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = "数据查询数据为空！",
                                Status = 400
                            }
                        });
                    }
                }
            }
            else
            {
                // 查询角色比表对应Id为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "角色Id获取异常，可能不存在！",
                        Status = 400
                    }
                });
            }
        }

        // 根据用户Id获取用户
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetUserById(int id)
        {
            // 判断传入的Id是否在用户表存在
            var user = _usersRepository.GetById(id);
            if (user != null)
            {
                // 查询的Id的用户表存在
                return JsonHelper.Serialize(new
                {
                    Data = user,
                    Meta = new
                    {
                        Msg = "用户数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的用户表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "用户数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 跳过token验证
        // [AllowAnonymous]
        // 添加新管理员用户
        [HttpPost]
        public dynamic AddUser(NewUser model)
        {
            // 用户名
            var username = model.Username.Trim();
            // 密码
            var password = model.Password.Trim();
            // 判断用户名和密码是否为空
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
            {
                // 用户名和密码为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 用户名和密码不为空
                var userInfo = _usersRepository.Table.Where(x => x.Username == username).Count();
                // 头像Id
                var avatarId = model.AvatarId;
                // 判断是否已创建用户
                if (userInfo == 0)
                {
                    // 用户未创建
                    // 角色Id
                    var roleId = model.RoleId;
                    // (1--超级管理员，2--供应商，3--普通用户)
                    if (roleId == 1)
                    {
                        // 超级管理员注册
                        // 默认创建用户昵称为用户名
                        var user = new Users
                        {
                            Username = username,
                            Password = password,
                            Nickname = username,
                            AvatarId = avatarId,
                            RoleId = 1
                        };
                        // 插入数据
                        _usersRepository.Insert(user);

                        // 查询对应角色Id对应的角色数据
                        var roles = _rolesRepository.Table.Where(x => x.Id == 1).ToList().FirstOrDefault();
                        // 统计对应角色在用户表中总数
                        var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == 1).Count();
                        // 对应角色用户总数
                        roles.UsersCount = usersCount;
                        // 更新角色表角色总数
                        _rolesRepository.Update(roles);

                        return JsonHelper.Serialize(new
                        {
                            Data = user,
                            Meta = new
                            {
                                Msg = "超级管理员账户创建成功！",
                                Status = 200
                            }
                        });
                    }
                    else if (roleId == 2)
                    {
                        // 供应商注册
                        // 默认创建用户昵称为用户名
                        var user = new Users
                        {
                            Username = username,
                            Password = password,
                            Nickname = username,
                            AvatarId = avatarId,
                            RoleId = 2
                        };
                        // 插入数据
                        _usersRepository.Insert(user);

                        // 查询对应角色Id对应的角色数据
                        var roles = _rolesRepository.Table.Where(x => x.Id == 2).ToList().FirstOrDefault();
                        // 统计对应角色在用户表中总数
                        var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == 2).Count();
                        // 对应角色用户总数
                        roles.UsersCount = usersCount;
                        // 更新角色表角色总数
                        _rolesRepository.Update(roles);

                        return JsonHelper.Serialize(new
                        {
                            Data = user,
                            Meta = new
                            {
                                Msg = "供应商账户创建成功！",
                                Status = 200
                            }
                        });
                    }
                    else if (roleId == 3)
                    {
                        // 普通用户注册
                        // 默认创建用户昵称为用户名
                        var user = new Users
                        {
                            Username = username,
                            Password = password,
                            Nickname = username,
                            AvatarId = avatarId,
                            RoleId = 3
                        };
                        // 插入数据
                        _usersRepository.Insert(user);

                        // 查询对应角色Id对应的角色数据
                        var roles = _rolesRepository.Table.Where(x => x.Id == 3).ToList().FirstOrDefault();
                        // 统计对应角色在用户表中总数
                        var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == 3).Count();
                        // 对应角色用户总数
                        roles.UsersCount = usersCount;
                        // 更新角色表角色总数
                        _rolesRepository.Update(roles);

                        return JsonHelper.Serialize(new
                        {
                            Data = user,
                            Meta = new
                            {
                                Msg = "普通账户创建成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 请求角色类型Id有误
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = "请求角色类型Id有误，请重试！",
                                Status = 400
                            }
                        });
                    }

                }
                else
                {
                    // 用户已存在
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "该用户已存在，无法重复创建！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // 跳过token验证
        [AllowAnonymous]
        // 添加新用户
        [HttpPost, Route("register")]
        public dynamic RegisterUser(NewUser model)
        {
            // 用户名
            var username = model.Username.Trim();
            // 密码
            var password = model.Password.Trim();
            // 注册用户类型（1--超级管理员，2--供货商，3--普通用户）
            var registerType = model.RegisterType == 0 ? 2 : model.RegisterType;
            // 判断用户名和密码是否为空
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
            {
                // 用户名和密码为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 用户名和密码为空
                var userInfo = _usersRepository.Table.Where(x => x.Username == username).Count();
                // // 头像Id
                // var avatarId = model.AvatarId;
                // 判断是否已创建用户
                if (userInfo == 0)
                {
                    // 用户未创建
                    // 默认创建用户昵称为用户名
                    var user = new Users
                    {
                        Username = username,
                        Password = password,
                        Nickname = username,
                        // AvatarId = avatarId,
                        RoleId = registerType
                    };
                    // 插入数据
                    _usersRepository.Insert(user);

                    // 查询对应角色Id对应的角色数据
                    var roles = _rolesRepository.Table.Where(x => x.Id == registerType).ToList().FirstOrDefault();
                    // 统计对应角色在用户表中总数
                    var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == 2).Count();
                    // 对应角色用户总数
                    roles.UsersCount = usersCount;
                    // 更新角色表角色总数
                    _rolesRepository.Update(roles);

                    return JsonHelper.Serialize(new
                    {
                        Data = user,
                        Meta = new
                        {
                            Msg = "创建成功！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 用户已存在
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "该用户已存在，无法重复创建！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // 用户信息修改
        [HttpPut("{id}")]
        public dynamic ModUser(int id, NewUser model)
        {
            var user = _usersRepository.GetById(id);
            // 判断所修改用户Id是否存在
            if (user != null)
            {
                // 要修改的用户Id存在
                // 请求修改的类型
                var modType = model.ModType;
                // 修改账户密码、配置角色
                if (modType == 0)
                {
                    var oldRolesId = user.RoleId;
                    // 用户名
                    user.Username = model.Username;
                    // 用户密码
                    user.Password = model.Password;
                    // 用户角色
                    user.RoleId = model.RoleId;
                    // 用户数据更新
                    _usersRepository.Update(user);

                    // 查询对应角色Id对应的角色数据
                    var oldRoles = _rolesRepository.Table.Where(x => x.Id == oldRolesId).ToList().FirstOrDefault();
                    // 统计对应角色在用户表中总数
                    var oldUsersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == oldRolesId).Count();
                    // 对应角色用户总数
                    oldRoles.UsersCount = oldUsersCount;
                    // 更新角色表角色总数
                    _rolesRepository.Update(oldRoles);

                    // 查询对应角色Id对应的角色数据
                    var roles = _rolesRepository.Table.Where(x => x.Id == model.RoleId).ToList().FirstOrDefault();
                    // 统计对应角色在用户表中总数
                    var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == model.RoleId).Count();
                    // 对应角色用户总数
                    roles.UsersCount = usersCount;
                    // 更新角色表角色总数
                    _rolesRepository.Update(roles);

                    return JsonHelper.Serialize(new
                    {
                        Data = user,
                        Meta = new
                        {
                            Msg = string.Format("你修改的用户的id为: {0} ，修改账户密码已经修改成功，请注意查收！", id),
                            Status = 200
                        }
                    });
                }
                // 修改个人中心信息
                else if (modType == 1)
                {
                    // 用户电话
                    user.TelephoneNumber = model.TelephoneNumber;
                    // 用户昵称
                    user.Nickname = model.Nickname;
                    // 用户头像Id
                    user.AvatarId = model.AvatarId;
                    // 用户邮箱
                    user.Email = model.Email;
                    // 用户地址
                    user.Address = model.Address;

                    // 用户数据更新
                    _usersRepository.Update(user);
                    return JsonHelper.Serialize(new
                    {
                        Data = user,
                        Meta = new
                        {
                            Msg = string.Format("你修改的用户的id为: {0} ，修改个人中心修改成功，请注意查收！", id),
                            Status = 200
                        }
                    });
                }
                // 修改密码
                else if (modType == 2)
                {
                    // 用户密码
                    user.Password = model.Password;
                    // 修改时间
                    user.UpdatedTime = DateTime.Now;
                    // 用户数据更新
                    _usersRepository.Update(user);
                    return JsonHelper.Serialize(new
                    {
                        Data = user,
                        Meta = new
                        {
                            Msg = string.Format("你修改的用户的id为: {0} ，修改密码修改成功，请注意查收！", id),
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 修改请求类型有误
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = string.Format("你修改的用户的id为: {0} ，请求修改的类型有误会，请确认你的请求后重试！", id),
                            Status = 400
                        }
                    });
                }

            }
            else
            {
                // 要修改的用户Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的用户不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 用户账户状态可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var user = _usersRepository.GetById(id);
            // 判断对应Id的用户是否存在
            if (user != null)
            {
                // 指定Id的用户存在
                _usersRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = user,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应用户的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的用户不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的用户不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 用户信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeleteUser(int id)
        {
            var user = _usersRepository.GetById(id);
            // 判断对应Id的用户是否存在
            if (user != null)
            {
                // 对应Id的用户存在
                // 伪删除操作
                _usersRepository.PseudoDeletion(id);

                // 查询对应角色Id对应的角色数据
                var roles = _rolesRepository.Table.Where(x => x.Id == 3).ToList().FirstOrDefault();
                // 统计对应角色在用户表中总数
                var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == 3).Count();
                // 对应角色用户总数
                roles.UsersCount = usersCount;
                // 更新角色表角色总数
                _rolesRepository.Update(roles);

                return JsonHelper.Serialize(new
                {
                    Data = user,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的数据,删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的用户不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的数据,无法执行删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 用户信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteUser(int id)
        {
            var user = _usersRepository.GetById(id);
            // 判断对应Id的用户是否存在
            if (user != null)
            {
                // 指定Id的用户存在
                // 彻底删除
                _usersRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = user,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的用户不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryUser(int id)
        {
            var user = _usersRepository.GetById(id);
            // 判断对应Id的用户是否存在
            if (user != null)
            {
                // 指定Id的用户存在
                // 恢复数据
                _usersRepository.Recovery(id);

                // 查询对应角色Id对应的角色数据
                var roles = _rolesRepository.Table.Where(x => x.Id == 3).ToList().FirstOrDefault();
                // 统计对应角色在用户表中总数
                var usersCount = _usersRepository.Table.ToList().Where(x => x.RoleId == 3).Count();
                // 对应角色用户总数
                roles.UsersCount = usersCount;
                // 更新角色表角色总数
                _rolesRepository.Update(roles);

                return JsonHelper.Serialize(new
                {
                    Data = user,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的用户的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 指定Id的用户不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的用户不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }


        // 跳过token验证
        [AllowAnonymous]
        // 用户登录获取token
        [HttpPost, Route("token")]
        public dynamic GetToken(NewUser model)
        {
            // 用户名
            var username = model.Username.Trim();
            // 用户密码
            var password = model.Password.Trim();
            // 在用户表查找用户
            var users = _usersRepository.Table.Where(x => x.Username == username).ToList().Count();
            // 判断用户是否存在
            if (users > 0)
            {
                // 用户存在
                // 在用户表中查找对应的用户名和密码
                var user =
                    _usersRepository
                        .Table
                        .Where(x =>
                            x.Username == username && x.Password == password)
                        .FirstOrDefault();
                // 在用户表中查找对应的用户名和密码获得的数据为空
                if (user == null)
                {
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "用户登录失败了,请确认密码是否正确！",
                            Status = 400
                        }
                    });
                }
                else
                {
                    // 在用户表中查找对应的用户名和密码获得的数据不为空
                    // 生成token
                    var token = TokenHelper.GenetateToken(_tokenParameter, user);

                    return JsonHelper.Serialize(new
                    {
                        Data = new { Token = token, User = user },
                        Meta = new
                        {
                            Msg = "用户登录成功！",
                            Status = 200
                        }
                    });
                }
            }
            else
            {
                // 用户不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "用户登录失败了,该用户尚未注册！",
                        Status = 401
                    }
                });
            }

        }
    }
}
