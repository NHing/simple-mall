﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CommodityParamsController : ControllerBase
    {
        // 定义调用商品参数表的接口
        private IRepository<CommodityParams> _commodityParamsRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public CommodityParamsController(IConfiguration configuration, IRepository<CommodityParams> commodityParamsRepository, IRepository<Commodities> commoditiesRepository, IRepository<Users> usersRepository)
        {
            _configuration = configuration;
            _commodityParamsRepository = commodityParamsRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 获取商品参数表数据
        [HttpGet]
        public dynamic GetCommodityParamsList([FromQuery] Pager pager)
        {
            // 页码（当前第几页）
            var pageIndex = pager.PageIndex;
            // 页码（当前第几页）
            var pageSize = pager.PageSize;

            // 判断商品查询时查询时商品层级（1--顶级，2--二级，3--三级）
            var type = pager.Type;


            // 获取商品参数表数据
            var commodityParams = _commodityParamsRepository.Table.ToList();
            // 判断商品参数表是否为空
            if (commodityParams != null)
            {
                // 商品参数表不为空
                // 判断是否进入回收站
                var recycle = pager.Recycle ? pager.Recycle : false;
                if (recycle)
                {
                    // 查看状态为删除商品参数表
                    var deletedCommodityParams = _commodityParamsRepository.DeleteTable;
                    // 给状态为删除的商品参数表分页
                    var deletedTable = deletedCommodityParams.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    return JsonHelper.Serialize(new
                    {
                        Data = new { DeletedCommodityParams = deletedTable, Pager = new { pageIndex, pageSize, PageTotal = deletedCommodityParams.Count() } },
                        Meta = new
                        {
                            Msg = "获取已删除商品参数列表成功！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 给状态为存在的商品参数表分页
                    var CommodityParams = commodityParams.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

                    return JsonHelper.Serialize(new
                    {
                        Data = new { CommodityParams = CommodityParams, Pager = new { pageIndex, pageSize, PageTotal = CommodityParams.Count() } },
                        Meta = new
                        {
                            Msg = "获取商品参数列表成功！",
                            Status = 200
                        }
                    });
                }
            }
            else
            {
                // 商品参数表为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品参数列表获取异常！",
                        Status = 400
                    }
                });
            }

        }

        // 跳过token认证
        [AllowAnonymous]
        // 获取子级分类层
        [HttpGet("type/{id}"), Route("type")]
        public dynamic GetCommodityParamByPid(int id)
        {
            // 判断传入的Id是否在商品参数表存在
            var commodityParams = _commodityParamsRepository.Table.ToList().Where(x => x.CommodityTypesId == id);
            if (commodityParams != null)
            {
                // 查询的Id的商品参数表存在
                return JsonHelper.Serialize(new
                {
                    Data = new { CommodityParams = commodityParams },
                    Meta = new
                    {
                        Msg = "商品参数数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的商品参数表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品参数数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据商品参数Id获取商品参数
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetCommodityParamById(int id)
        {
            // 判断传入的Id是否在商品参数表存在
            var commodityParams = _commodityParamsRepository.GetById(id);
            if (commodityParams != null)
            {
                // 查询的Id的商品参数表存在
                return JsonHelper.Serialize(new
                {
                    Data = commodityParams,
                    Meta = new
                    {
                        Msg = "商品参数数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的商品参数表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品参数数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 添加新商品参数
        [HttpPost("{id}")]
        public dynamic AddCommodityParam(int id, NewCommodityParam model)
        {
            // 商品参数名称
            var commodityParamName = model.CommodityParamName;
            // 商品参数父级Id
            var commodityTypesId = id;
            // 商品参数子级
            var paramChildren = model.ParamChildren;

            if (string.IsNullOrEmpty(paramChildren))
            {
                // 商品参数名称不为空
                var CommodityParam = _commodityParamsRepository.Table.Where(x => x.CommodityParamName == commodityParamName && x.CommodityTypesId == id).Count();
                // 判断要创建的商品参数不存在
                if (CommodityParam == 0)
                {
                    // 创建新商品参数
                    var commodityParams = new CommodityParams
                    {
                        CommodityParamName = commodityParamName,
                        CommodityTypesId = commodityTypesId
                    };
                    // 商品参数插入
                    _commodityParamsRepository.Insert(commodityParams);
                    return JsonHelper.Serialize(new
                    {
                        Data = commodityParams,
                        Meta = new
                        {
                            Msg = "创建新商品参数成功！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 商品参数已经存在了
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "该商品参数已存在，无法重复创建！",
                            Status = 400
                        }
                    });
                }
            }
            else
            {
                // 判断商品参数名称是否为空
                if (string.IsNullOrEmpty(commodityParamName))
                {
                    // 商品参数名称为空时
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "插入的商品参数名称不能为空，请确认后重试！",
                            Status = 400
                        }
                    });
                }
                else
                {
                    // 商品参数名称不为空
                    var CommodityParam = _commodityParamsRepository.Table.Where(x => x.CommodityParamName == commodityParamName && x.CommodityTypesId == id).Count();
                    // 判断要创建的商品参数不存在
                    if (CommodityParam == 0)
                    {
                        // 创建新商品参数
                        var commodityParams = new CommodityParams
                        {
                            CommodityParamName = commodityParamName,
                            CommodityTypesId = commodityTypesId,
                            ParamChildren = paramChildren
                        };
                        // 商品参数插入
                        _commodityParamsRepository.Insert(commodityParams);
                        return JsonHelper.Serialize(new
                        {
                            Data = commodityParams,
                            Meta = new
                            {
                                Msg = "创建新商品参数成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 商品参数已经存在了
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = "该商品参数已存在，无法重复创建！",
                                Status = 400
                            }
                        });
                    }
                }

            }
        }

        // 商品参数信息修改
        [HttpPut("{id}")]
        public dynamic ModCommodityParam(int id, NewCommodityParam model)
        {
            var commodityParams = _commodityParamsRepository.GetById(id);
            // 判断所修改商品参数Id是否存在
            if (commodityParams != null)
            {
                // 商品参数对应商品类别Id
                var commodityTypesId = model.CommodityTypesId;
                // 商品参数子级
                var paramChildren = model.ParamChildren;
                if (!string.IsNullOrEmpty(paramChildren))
                {
                    // 统计要修改的商品参数名称在商品参数表中的数量
                    var commodityParamsCount = _commodityParamsRepository.Table.Where(x => x.CommodityParamName == model.CommodityParamName && x.CommodityTypesId == commodityTypesId).ToList().Count();
                    // 判断要修改的商品参数名称是否已经创建过了
                    if (commodityParamsCount == 0)
                    {
                        // 要修改的名称未创建
                        // 商品参数更新
                        commodityParams.ParamChildren = paramChildren;
                        _commodityParamsRepository.Update(commodityParams);
                        return JsonHelper.Serialize(new
                        {
                            Data = commodityParams,
                            Meta = new
                            {
                                Msg = string.Format("你修改的商品参数的id为: {0} ，已经修改成功，请注意查收！", id),
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 要修改的名称已创建
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = string.Format("你修改的商品参数的id为: {0} ，要修改的商品参数名称已存在，请尝试修改为其他名称！", id),
                                Status = 400
                            }
                        });
                    }
                }
                else
                {
                    // 统计要修改的商品参数名称在商品参数表中的数量
                    var commodityParamsCount = _commodityParamsRepository.Table.Where(x => x.CommodityParamName == model.CommodityParamName && x.CommodityTypesId == commodityTypesId).ToList().Count();
                    // 判断要修改的商品参数名称是否已经创建过了
                    if (commodityParamsCount == 0 && !string.IsNullOrEmpty(model.CommodityParamName))
                    {
                        // 要修改的名称未创建
                        // 商品参数名称
                        commodityParams.CommodityParamName = model.CommodityParamName;
                        // 商品参数更新
                        _commodityParamsRepository.Update(commodityParams);
                        return JsonHelper.Serialize(new
                        {
                            Data = commodityParams,
                            Meta = new
                            {
                                Msg = string.Format("你修改的商品参数的id为: {0} ，已经修改成功，请注意查收！", id),
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 要修改的名称已创建
                        return JsonHelper.Serialize(new
                        {
                            Data = "",
                            Meta = new
                            {
                                Msg = string.Format("你修改的商品参数的id为: {0} ，要修改的商品参数名称已存在，请尝试修改为其他名称！", id),
                                Status = 400
                            }
                        });
                    }
                }
            }
            else
            {
                // 该商品参数Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品参数不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 商品参数状态可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var commodityParams = _commodityParamsRepository.GetById(id);
            // 判断对应Id的商品参数是否存在
            if (commodityParams != null)
            {
                // 指定Id的商品参数存在
                _commodityParamsRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityParams,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应商品参数的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的商品参数不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品参数不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 商品参数信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeleteCommodityParam(int id)
        {
            var commodityParams = _commodityParamsRepository.GetById(id);
            // 判断对应Id的商品参数是否存在
            if (commodityParams != null)
            {
                // 对应Id的商品参数存在
                // 伪删除操作
                _commodityParamsRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityParams,
                    Meta = new
                    {
                        Msg = string.Format("你删除的商品参数Id为 {0} 的数据,伪删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的商品参数不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的商品参数Id为 {0} 的数据,执行伪删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 商品参数信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteCommodityParam(int id)
        {
            var commodityParams = _commodityParamsRepository.GetById(id);
            // 判断对应Id的商品参数是否存在
            if (commodityParams != null)
            {   // 指定Id的商品参数存在
                // 彻底删除
                _commodityParamsRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityParams,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的商品参数数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的商品参数不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的商品参数数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的商品参数数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryCommodityParam(int id)
        {
            var commodityParams = _commodityParamsRepository.GetById(id);
            // 判断对应Id的商品参数是否存在
            if (commodityParams != null)
            {
                // 指定Id的商品参数存在
                // 恢复数据
                _commodityParamsRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityParams,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的商品参数的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 指定Id的商品参数不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品参数不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
