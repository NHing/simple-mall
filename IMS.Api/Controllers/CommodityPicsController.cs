﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CommodityPicsController : ControllerBase
    {
        // 定义调用商品图片表的接口
        private IRepository<CommodityPics> _commodityPicsRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public CommodityPicsController(IConfiguration configuration, IRepository<CommodityPics> commodityPicsRepository)
        {
            _configuration = configuration;
            _commodityPicsRepository = commodityPicsRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 跳过token认证
        [AllowAnonymous]
        // 商品图片信息数据
        [HttpGet]
        public dynamic GetCommodityPicList()
        {
            var commodityPic = _commodityPicsRepository.Table.ToList();
            // 判断商品图片信息是否为空
            if (commodityPic != null)
            {
                // 商品图片信息不为空
                return JsonHelper.Serialize(new
                {
                    Data = new { CommodityPics = commodityPic },
                    Meta = new
                    {
                        Msg = "获取商品图片信息成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 商品图片信息为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品图片信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据Id获取商品图片信息
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetCommodityPicById(int id)
        {
            var commodityPic = _commodityPicsRepository.GetById(id);
            // 判断传入的Id是否在商品图片表中存在
            if (commodityPic != null)
            {
                // 传入的Id在商品图片表中存在
                return JsonHelper.Serialize(new
                {
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = "商品图片指定Id信息获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的Id在商品图片表中不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品图片指定Id信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据PicId获取商品图片信息
        [HttpGet("picid/{picid}"), Route("picid")]
        public dynamic GetCommodityPicByPicId(int picid)
        {
            var commodityPic = _commodityPicsRepository.Table.Where(x => x.PicId == picid).FirstOrDefault();
            // 判断传入的PicId是否在商品图片表中存在
            if (commodityPic != null)
            {
                // 传入的PicId在商品图片表中存在
                return JsonHelper.Serialize(new
                {
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = "商品图片指定PicId信息获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的PicId在商品图片表中不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品图片指定PicId信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 跳过token验证(初始化数据库后可短暂取消忽略，创建初始号后在进行忽略)
        // [AllowAnonymous]
        // 添加商品图片图片
        [HttpPost]
        public dynamic AddCommodityPic(NewPicture model)
        {
            // 判断插入商品图片图片Id是否异常
            if (model.PicId < 0)
            {
                // 插入的图片id<0
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入的商品图片信息不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 插入的图片id>0
                var commodityPic = new CommodityPics
                {
                    PicId = model.PicId
                };
                // 插入数据
                _commodityPicsRepository.Insert(commodityPic);
                return JsonHelper.Serialize(new
                {
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = "添加商品图片图片成功！",
                        Status = 200
                    }
                });
            }
        }

        // 商品图片信息修改
        [HttpPut("{id}")]
        public dynamic ModCommodityPic(int id, NewPicture model)
        {
            var commodityPic = _commodityPicsRepository.GetById(id);
            // 判断所修改商品图片Id是否存在
            if (commodityPic != null)
            {
                // 修改商品图片对应的Id存在
                // 商品图片图片Id
                commodityPic.PicId = model.PicId;

                // 更新数据
                _commodityPicsRepository.Update(commodityPic);
                return JsonHelper.Serialize(new
                {
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = string.Format("你修改的商品图片信息对应的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 修改商品图片对应的Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品图片信息不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 商品图片信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeletecommodityPics(int id)
        {
            var commodityPic = _commodityPicsRepository.GetById(id);
            // 判断对应Id的商品图片是否存在
            if (commodityPic != null)
            {
                // 对应Id的商品图片存在
                // 伪删除操作
                _commodityPicsRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = string.Format("你删除的商品图片信息Id为 {0} 的数据,删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的商品图片不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的商品图片信息Id为 {0} 的数据,执行删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 商品图片信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeletecommodityPics(int id)
        {
            var commodityPic = _commodityPicsRepository.GetById(id);
            // 判断对应Id的商品图片是否存在
            if (commodityPic != null)
            {
                // 对应Id的商品图片存在
                // 彻底删除
                _commodityPicsRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的商品图片信息数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的商品图片不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的商品图片信息数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的商品图片信息数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryWebsiteInfo(int id)
        {
            var commodityPic = _commodityPicsRepository.GetById(id);
            // 判断对应Id的商品图片是否存在
            if (commodityPic != null)
            {
                // 对应Id的商品图片存在
                // 恢复数据
                _commodityPicsRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Code = 1000,
                    Data = commodityPic,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的商品图片信息的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 对应Id的商品图片不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品图片信息不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
