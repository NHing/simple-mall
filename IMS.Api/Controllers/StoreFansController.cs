﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class StoreFansController : ControllerBase
    {
        // 定义调用店铺粉丝关注表的接口
        private IRepository<StoreFans> _storeFansRepository;
        // 定义调用店铺表的接口
        private IRepository<Stores> _storeRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public StoreFansController(IConfiguration configuration, IRepository<StoreFans> storeFansRepository, IRepository<Stores> storeRepository)
        {
            _configuration = configuration;
            _storeFansRepository = storeFansRepository;
            _storeRepository = storeRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 跳过token验证
        // [AllowAnonymous]
        // 获取店铺粉丝关注表数据
        [HttpGet]
        public dynamic GetStoreFanList([FromQuery] Pager pager)
        {
            // 页码（当前第几页）
            var pageIndex = pager.PageIndex;
            // 页码（当前第几页）
            var pageSize = pager.PageSize;

            var storeFan = _storeFansRepository.Table.ToList();
            // 判断店铺粉丝关注表是否为空
            if (storeFan != null)
            {
                // 店铺粉丝关注表不为空
                // 给状态为存在的店铺粉丝关注表分页
                var activedTable = storeFan.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                return JsonHelper.Serialize(new
                {
                    Data = new { StoreFans = activedTable, Pager = new { pageIndex, pageSize, PageTotal = storeFan.Count() } },
                    Meta = new
                    {
                        Msg = "获取店铺粉丝关注列表成功！",
                        Status = 200
                    }
                });

            }
            else
            {
                // 店铺粉丝关注表为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "店铺粉丝关注列表获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据店铺Id和用户Id获取店铺粉丝关注信息
        [HttpGet(), Route("storeid")]
        public dynamic GetStoreFanByStoreId([FromQuery] NewFan model)
        {
            var fromStoreId = model.FromStoreId;
            var fromUserId = model.FromUserId;
            // 判断传入的店铺Id和用户Id对应的店铺粉丝关注表是否存在
            var storeFan = _storeFansRepository.Table.Where(x => x.FromStoreId == fromStoreId && x.FromUserId == fromUserId).ToList();
            if (storeFan != null)
            {
                // 传入的店铺Id和用户Id对应的店铺粉丝关注表存在
                return JsonHelper.Serialize(new
                {
                    Data = storeFan,
                    Meta = new
                    {
                        Msg = "根据店铺Id和店铺粉丝关注Id获取店铺粉丝关注数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的店铺Id和用户Id对应的店铺粉丝关注表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "店铺粉丝关注数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据店铺粉丝关注Id获取店铺粉丝关注
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetStoreFanById(int id)
        {
            // 判断传入的Id是否在店铺粉丝关注表存在
            var storeFan = _storeFansRepository.GetById(id);
            if (storeFan != null)
            {
                // 查询的Id的店铺粉丝关注表存在
                return JsonHelper.Serialize(new
                {
                    Data = storeFan,
                    Meta = new
                    {
                        Msg = "店铺粉丝关注数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的店铺粉丝关注表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "店铺粉丝关注数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 添加新店铺粉丝关注
        [HttpPost]
        public dynamic AddStoreFan(NewFan model)
        {
            // 店铺Id
            var fromStoreId = model.FromStoreId;
            // 店铺粉丝关注人id
            var fromuserId = model.FromUserId;

            // 获取对应店铺对应店铺粉丝关注信息
            var storeFans = _storeFansRepository.Table.Where(x => x.FromStoreId == fromStoreId && x.FromUserId == fromuserId).ToList();
            // 判断对应店铺对应店铺粉丝关注信息是否存在
            var storeFansCount = storeFans.Count();
            if (storeFansCount > 0)
            {
                var storeFan = storeFans.FirstOrDefault();
                // 点过赞了
                var storeFanStatus = storeFan.FocusStatus;
                if (storeFanStatus == 0)
                {
                    // 店铺粉丝关注
                    storeFan.FocusStatus = 1;
                    // 店铺粉丝关注表数据更新
                    _storeFansRepository.Update(storeFan);

                    // 根据店铺Id获取店铺信息
                    var store = _storeRepository.GetById(fromStoreId);
                    // 统计对应店铺店铺粉丝关注表所有店铺粉丝关注的
                    var storeStoreFanCount = _storeFansRepository.Table.ToList().Where(x => x.FocusStatus == 1 && x.FromStoreId == fromStoreId).Count();
                    // 对应店铺店铺粉丝关注数
                    store.StoreFans = storeStoreFanCount;
                    // 店铺表店铺粉丝关注数更新
                    _storeRepository.Update(store);

                    return JsonHelper.Serialize(new
                    {
                        Data = storeFan,
                        Meta = new
                        {
                            Msg = "店铺粉丝关注成功！！！",
                            Status = 200
                        }
                    });
                }
                else if (storeFanStatus == 1)
                {
                    // 取消赞
                    storeFan.FocusStatus = 0;
                    // 店铺粉丝关注表数据更新
                    _storeFansRepository.Update(storeFan);

                    // 根据店铺Id获取店铺信息
                    var store = _storeRepository.GetById(fromStoreId);
                    // 统计对应店铺店铺粉丝关注表所有店铺粉丝关注的
                    var storeStoreFanCount = _storeFansRepository.Table.ToList().Where(x => x.FocusStatus == 1 && x.FromStoreId == fromStoreId).Count();
                    // 对应店铺店铺粉丝关注数
                    store.StoreFans = storeStoreFanCount;
                    // 店铺表店铺粉丝关注数更新
                    _storeRepository.Update(store);

                    return JsonHelper.Serialize(new
                    {
                        Data = storeFan,
                        Meta = new
                        {
                            Msg = "取消赞成功！！！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 店铺粉丝关注状态有误
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "店铺粉丝关注状态有误！！！",
                            Status = 400
                        }
                    });
                }
            }
            else
            {
                // 还没有点过赞
                var storeFan = new StoreFans
                {
                    FromStoreId = fromStoreId,
                    FromUserId = fromuserId,
                    FocusStatus = 1
                };
                // 插入数据
                _storeFansRepository.Insert(storeFan);

                // 根据店铺Id获取店铺信息
                var store = _storeRepository.GetById(fromStoreId);
                // 统计对应店铺店铺粉丝关注表所有店铺粉丝关注的
                var storeStoreFanCount = _storeFansRepository.Table.ToList().Where(x => x.FocusStatus == 1 && x.FromStoreId == fromStoreId).Count();
                // 对应店铺店铺粉丝关注数
                store.StoreFans = storeStoreFanCount;
                // 店铺表店铺粉丝关注数更新
                _storeRepository.Update(store);

                return JsonHelper.Serialize(new
                {
                    Data = storeFan,
                    Meta = new
                    {
                        Msg = "添加店铺粉丝关注成功！",
                        Status = 200
                    }
                });
            }
        }
    }
}
