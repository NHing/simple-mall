﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class RolesController : ControllerBase
    {
        // 定义调用用户角色表的接口
        private IRepository<Roles> _rolesRepository;
        // 定义调用用户表的接口
        private IRepository<Users> _usersRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public RolesController(IConfiguration configuration, IRepository<Roles> rolesRepository, IRepository<Users> usersRepository)
        {
            _configuration = configuration;
            _rolesRepository = rolesRepository;
            _usersRepository = usersRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 获取角色表数据
        [HttpGet]
        public dynamic GetRolesList([FromQuery] Pager pager)
        {
            // 页码（当前第几页）
            var pageIndex = pager.PageIndex;
            // 页码（当前第几页）
            var pageSize = pager.PageSize;
            // 查询的内容
            var query = pager.Query;
            // 判断是否查询
            if (string.IsNullOrEmpty(query))
            {
                // 查询内容为空
                // 获取角色表数据
                var roles = _rolesRepository.Table.ToList();
                // 判断角色表是否为空
                if (roles != null)
                {
                    // 角色表不为空
                    // 判断是否进入回收站
                    var recycle = pager.Recycle ? pager.Recycle : false;
                    if (recycle)
                    {
                        // 查看状态为删除角色表
                        var deletedRoles = _rolesRepository.DeleteTable;
                        // 给状态为删除的角色表分页
                        var deletedTable = deletedRoles.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(); ;
                        return JsonHelper.Serialize(new
                        {
                            Data = new { DeletedRoles = deletedTable, Pager = new { pageIndex, pageSize, PageTotal = deletedRoles.Count() } },
                            Meta = new
                            {
                                Msg = "获取已删除角色列表成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 给状态为存在的角色表分页
                        var activedTable = roles.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                        return JsonHelper.Serialize(new
                        {
                            Data = new { Roles = activedTable, Pager = new { pageIndex, pageSize, PageTotal = roles.Count() } },
                            Meta = new
                            {
                                Msg = "获取角色列表成功！",
                                Status = 200
                            }
                        });
                    }
                }
                else
                {
                    // 角色表为空
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "角色列表获取异常！",
                            Status = 400
                        }
                    });
                }

            }
            else
            {
                // 查询内容不为空
                // 根据用户角色名称查找
                var roles = _rolesRepository.Table.ToList().Where(x => x.RoleName.Contains(query));
                // 判断查询的角色是否为空
                if (roles != null)
                {
                    // 查询的角色不为空
                    // 给查询到的内容分页
                    var activedTable = roles.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                    return JsonHelper.Serialize(new
                    {
                        Data = new { Roles = activedTable, Pager = new { pageIndex, pageSize, PageTotal = roles.Count() } },
                        Meta = new
                        {
                            Msg = "文章角色查询成功！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 查询的角色为空
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "没有找你你要查找的角色！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // 根据角色Id获取角色
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetArticleRolesById(int id)
        {
            // 判断传入的Id是否在角色表存在
            var roles = _rolesRepository.GetById(id);
            if (roles != null)
            {
                // 查询的Id的角色表存在
                return JsonHelper.Serialize(new
                {
                    Data = roles,
                    Meta = new
                    {
                        Msg = "角色数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的角色表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "角色数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 添加新角色
        [HttpPost]
        public dynamic AddRoles(NewRole model)
        {
            // 角色名称
            var category = model.RoleName;
            // 统计用户表下对应角色的文章总数量（默认为0）
            var usersCount = 0;
            // 角色创建人
            var creator = _usersRepository.Table.Where(x => x.Id == model.UserId).ToList()[0].Username;
            // 角色备注
            var remarks = !string.IsNullOrEmpty(model.Remarks) ? model.Remarks : null;

            // 判断角色名称是否为空
            if (string.IsNullOrEmpty(model.RoleName))
            {
                // 角色名称为空时
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入的角色名称不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 角色名称不为空
                var Roles = _rolesRepository.Table.Where(x => x.RoleName == category).Count();
                // 判断要创建的角色不存在
                if (Roles == 0)
                {
                    // 创建新角色
                    var roles = new Roles
                    {
                        RoleName = category,
                        UsersCount = usersCount,
                        Creator = creator,
                        Remarks = remarks
                    };
                    // 角色插入
                    _rolesRepository.Insert(roles);
                    return JsonHelper.Serialize(new
                    {
                        Data = roles,
                        Meta = new
                        {
                            Msg = "创建新角色成功！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 角色已经存在了
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "该角色已存在，无法重复创建！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // 角色信息修改
        [HttpPut("{id}")]
        public dynamic ModArticleRoles(int id, NewRole model)
        {
            var roles = _rolesRepository.GetById(id);
            // 判断所修改角色Id是否存在
            if (roles != null)
            {
                // 角色名称
                roles.RoleName = model.RoleName;
                // 角色备注
                roles.Remarks = !string.IsNullOrEmpty(model.Remarks) ? model.Remarks : null;
                // 更新时间
                roles.UpdatedTime = DateTime.Now;
                // 统计要修改的名称在角色表中的数量
                var rolesCount = _rolesRepository.Table.Where(x => x.RoleName == model.RoleName).ToList().Count();
                // 判断要修改的角色名称是否已经创建过了
                if (rolesCount == 0)
                {
                    // 要修改的名称未创建
                    // 角色更新
                    _rolesRepository.Update(roles);
                    return JsonHelper.Serialize(new
                    {
                        Data = roles,
                        Meta = new
                        {
                            Msg = string.Format("你修改的角色的id为: {0} ，已经修改成功，请注意查收！", id),
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 要修改的名称已创建
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = string.Format("你修改的角色的id为: {0} ，要修改的角色名称已存在，请尝试修改为其他名称！", id),
                            Status = 400
                        }
                    });
                }

            }
            else
            {
                // 该角色Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的角色不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 角色状态可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var roles = _rolesRepository.GetById(id);
            // 判断对应Id的角色是否存在
            if (roles != null)
            {
                // 指定Id的角色存在
                _rolesRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = roles,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应角色的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的角色不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的角色不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 角色信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeleteArticleRoles(int id)
        {
            var roles = _rolesRepository.GetById(id);
            // 判断对应Id的角色是否存在
            if (roles != null)
            {
                // 对应Id的角色存在
                // 伪删除操作
                _rolesRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = roles,
                    Meta = new
                    {
                        Msg = string.Format("你删除的角色Id为 {0} 的数据,伪删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的角色不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的角色Id为 {0} 的数据,执行伪删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 角色信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteArticleRoles(int id)
        {
            var roles = _rolesRepository.GetById(id);
            // 判断对应Id的角色是否存在
            if (roles != null)
            {   // 指定Id的角色存在
                // 彻底删除
                _rolesRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = roles,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的角色数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的角色不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的角色数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的角色数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryArticleRoles(int id)
        {
            var roles = _rolesRepository.GetById(id);
            // 判断对应Id的角色是否存在
            if (roles != null)
            {
                // 指定Id的角色存在
                // 恢复数据
                _rolesRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Data = roles,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的角色的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 指定Id的角色不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的角色不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
