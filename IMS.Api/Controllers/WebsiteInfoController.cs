﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WebsiteInfoController : ControllerBase
    {
        // 定义调用文章信息表的接口
        private IRepository<WebsiteInfo> _websiteInfoRepository;
        // 定义调用用户表的接口
        private IRepository<Users> _usersRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public WebsiteInfoController(IConfiguration configuration, IRepository<Users> usersRepository, IRepository<WebsiteInfo> websiteInfoRepository)
        {
            _configuration = configuration;
            _usersRepository = usersRepository;
            _websiteInfoRepository = websiteInfoRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 跳过token认证
        [AllowAnonymous]
        // 网站信息数据
        [HttpGet]
        public dynamic GetWebsiteInfoList()
        {
            var websiteInfo = _websiteInfoRepository.Table.ToList();
            // 判断网站信息是否为空
            if (websiteInfo != null)
            {
                // 网站信息不为空
                return JsonHelper.Serialize(new
                {
                    Data = new { WebsiteInfo = websiteInfo },
                    Meta = new
                    {
                        Msg = "获取网站信息成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 网站信息为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "网站信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据栏目Id获取网站信息
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetWebsiteInfoById(int id)
        {
            var websiteInfo = _websiteInfoRepository.GetById(id);
            // 判断传入的Id是否在网站信息表中存在
            if (websiteInfo != null)
            {
                // 传入的Id在网站信息表中存在
                return JsonHelper.Serialize(new
                {
                    Data = websiteInfo,
                    Meta = new
                    {
                        Msg = "网站指定Id信息获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 传入的Id在网站信息表中不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "网站指定Id信息获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 跳过token验证(初始化数据库后可短暂取消忽略，创建初始号后在进行忽略)
        // [AllowAnonymous]
        // 添加网站信息
        [HttpPost]
        public dynamic AddWebsiteInfo(NewWebsiteInfo model)
        {
            
            // 网站LogoId
            var websiteLogoId = model.WebsiteLogoId;
            // 网站名称
            var websiteName = model.WebsiteName;
            // 域名
            var domainName = model.DomainName;
            // ICP备案
            var ICPNumber = model.ICPNumber;
            // 公安备案
            var policeNumber = model.PoliceNumber;
            // 判断插入网站信息是否为空
            if (string.IsNullOrEmpty(websiteName) && string.IsNullOrEmpty(domainName) && string.IsNullOrEmpty(ICPNumber) && string.IsNullOrEmpty(policeNumber))
            {
                // 插入的数据为空
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入的网站信息不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 插入的数据不为空
                var websiteInfo = new WebsiteInfo
                {
                    WebsiteLogoId =websiteLogoId,
                    WebsiteName = websiteName,
                    DomainName = domainName,
                    ICPNumber = ICPNumber,
                    PoliceNumber = policeNumber
                };
                // 插入数据
                _websiteInfoRepository.Insert(websiteInfo);
                return JsonHelper.Serialize(new
                {
                    Data = websiteInfo,
                    Meta = new
                    {
                        Msg = "创建网站信息成功！",
                        Status = 200
                    }
                });
            }
        }

        // 网站信息修改
        [HttpPut("{id}")]
        public dynamic ModWebsiteInfo(int id, NewWebsiteInfo model)
        {
            var websiteInfo = _websiteInfoRepository.GetById(id);
            // 判断所修改网站Id是否存在
            if (websiteInfo != null)
            {
                // 该id对应的网站信息数据存在
                // 网站LogoId
                websiteInfo.WebsiteLogoId = model.WebsiteLogoId;
                // 网站名称
                websiteInfo.WebsiteName = model.WebsiteName;
                // 网站域名
                websiteInfo.DomainName = model.DomainName;
                // ICP备案
                websiteInfo.ICPNumber = model.ICPNumber;
                // 公安备案
                websiteInfo.PoliceNumber = model.PoliceNumber;
                // 更新时间
                websiteInfo.UpdatedTime = DateTime.Now;
                // 更新数据
                _websiteInfoRepository.Update(websiteInfo);
                return JsonHelper.Serialize(new
                {
                    Data = websiteInfo,
                    Meta = new
                    {
                        Msg = string.Format("你修改的网站信息对应的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 该id对应的网站信息数据不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的网站信息不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 网站信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeleteWebsiteInfo(int id)
        {
            var websiteInfo = _websiteInfoRepository.GetById(id);
            // 判断对应Id的网站信息是否存在
            if (websiteInfo != null)
            {
                // 对应Id的网站信息存在
                // 伪删除操作
                _websiteInfoRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = websiteInfo,
                    Meta = new
                    {
                        Msg = string.Format("你删除的网站信息Id为 {0} 的数据,伪删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的网站信息不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的网站信息Id为 {0} 的数据,执行伪删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 网站信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteWebsiteInfo(int id)
        {
            var websiteInfo = _websiteInfoRepository.GetById(id);
            // 判断对应Id的网站信息是否存在
            if (websiteInfo != null)
            {
                // 对应Id的网站信息存在
                // 彻底删除
                _websiteInfoRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = websiteInfo,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的网站信息数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的网站信息不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的网站信息数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的网站信息数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryWebsiteInfo(int id)
        {
            var websiteInfo = _websiteInfoRepository.GetById(id);
            // 判断对应Id的网站信息是否存在
            if (websiteInfo != null)
            {
                // 对应Id的网站信息存在
                // 恢复数据
                _websiteInfoRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Data = websiteInfo,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的网站信息的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 对应Id的网站信息不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的网站信息不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
