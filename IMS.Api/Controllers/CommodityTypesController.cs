﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IMS.Api.Repository;
using IMS.Api.Entity;
using Microsoft.Extensions.Configuration;
using IMS.Api.Utils;
using IMS.Api.ParamModel;
using Microsoft.AspNetCore.Authorization;

namespace IMS.Api.Controllers
{
    // 全局验证token
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CommodityTypesController : ControllerBase
    {
        // 定义调用商品类型表的接口
        private IRepository<CommodityTypes> _commodityTypesRepository;
        // 定义调用文章表的接口
        private IRepository<Commodities> _commoditiesRepository;
        // 定义调用用户表的接口
        private IRepository<Users> _usersRepository;
        // 定义token
        private TokenParameter _tokenParameter;
        // 定义配置访问接口
        private readonly IConfiguration _configuration;

        // 依赖注入
        public CommodityTypesController(IConfiguration configuration, IRepository<CommodityTypes> commodityTypesRepository, IRepository<Commodities> commoditiesRepository, IRepository<Users> usersRepository)
        {
            _configuration = configuration;
            _commodityTypesRepository = commodityTypesRepository;
            _commoditiesRepository = commoditiesRepository;
            _usersRepository = usersRepository;
            // Token
            _tokenParameter =
                configuration
                    .GetSection("tokenParameter")
                    .Get<TokenParameter>();
        }

        // 获取商品类型表数据
        [HttpGet]
        public dynamic GetCommodityTypesList([FromQuery] Pager pager)
        {
            // 页码（当前第几页）
            var pageIndex = pager.PageIndex;
            // 页码（当前第几页）
            var pageSize = pager.PageSize;

            // 判断商品查询时查询时商品层级（1--顶级，2--二级，3--三级）
            var type = pager.Type;
            // 顶级
            if (type == 1)
            {
                var commodityType = _commodityTypesRepository.Table.ToList().Where(x => x.CommodityLevel == 0);
                // 层级类型返回
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = "顶级类型获取成功！",
                        Status = 200
                    }
                });
            }
            // 二级
            else if (type == 2)
            {
                var commodityType = _commodityTypesRepository.Table.ToList().Where(x => x.CommodityLevel == 1);
                // 层级类型返回
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = "二级类型获取成功！",
                        Status = 200
                    }
                });
            }
            // 三级
            else if (type == 3)
            {
                var commodityType = _commodityTypesRepository.Table.ToList().Where(x => x.CommodityLevel == 2);
                // 层级类型返回
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = "三级类型获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 获取商品类型表数据
                var commodityType = _commodityTypesRepository.Table.ToList();
                // 判断商品类型表是否为空
                if (commodityType != null)
                {
                    // 商品类型表不为空
                    // 判断是否进入回收站
                    var recycle = pager.Recycle ? pager.Recycle : false;
                    if (recycle)
                    {
                        // 查看状态为删除商品类型表
                        var deletedCommodityTypes = _commodityTypesRepository.DeleteTable;
                        // 给状态为删除的商品类型表分页
                        var deletedTable = deletedCommodityTypes.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                        return JsonHelper.Serialize(new
                        {
                            Data = new { DeletedCommodityTypes = deletedTable, Pager = new { pageIndex, pageSize, PageTotal = deletedCommodityTypes.Count() } },
                            Meta = new
                            {
                                Msg = "获取已删除商品类型列表成功！",
                                Status = 200
                            }
                        });
                    }
                    else
                    {
                        // 给状态为存在的商品类型表分页
                        var CommodityTypes = commodityType.Where(x => x.CommodityLevel == 0).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

                        return JsonHelper.Serialize(new
                        {
                            Data = new { CommodityTypes = CommodityTypes, Pager = new { pageIndex, pageSize, PageTotal = CommodityTypes.Count() } },
                            Meta = new
                            {
                                Msg = "获取商品类型列表成功！",
                                Status = 200
                            }
                        });
                    }
                }
                else
                {
                    // 商品类型表为空
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "商品类型列表获取异常！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // 获取子级分类层
        [HttpGet("pid/{id}"), Route("pid")]
        public dynamic GetCommodityTypeByPid(int id)
        {
            // 判断传入的Id是否在商品类型表存在
            var commodityType = _commodityTypesRepository.Table.ToList().Where(x => x.CommodityPid == id);
            if (commodityType != null)
            {
                // 查询的Id的商品类型表存在
                return JsonHelper.Serialize(new
                {
                    Data = new { CommodityTypes = commodityType },
                    Meta = new
                    {
                        Msg = "商品类型数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的商品类型表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品类型数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 根据商品类型Id获取商品类型
        [HttpGet("id/{id}"), Route("id")]
        public dynamic GetCommodityTypeById(int id)
        {
            // 判断传入的Id是否在商品类型表存在
            var commodityType = _commodityTypesRepository.GetById(id);
            if (commodityType != null)
            {
                // 查询的Id的商品类型表存在
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = "商品类型数据获取成功！",
                        Status = 200
                    }
                });
            }
            else
            {
                // 查询的Id的商品类型表不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "商品类型数据获取异常！",
                        Status = 400
                    }
                });
            }
        }

        // 添加新商品类型
        [HttpPost]
        public dynamic AddCommodityType(NewCommodityType model)
        {
            // 商品类型名称
            var category = model.CommodityTypeName;
            // 商品类型父级Id
            var commodityPid = model.CommodityPid;
            // 商品类型层级（0--顶级，1--二级，2--三级）
            var commodityLevel = model.CommodityLevel;

            // 判断商品类型名称是否为空
            if (string.IsNullOrEmpty(model.CommodityTypeName))
            {
                // 商品类型名称为空时
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "插入的商品类型名称不能为空，请确认后重试！",
                        Status = 400
                    }
                });
            }
            else
            {
                // 商品类型名称不为空
                var CommodityType = _commodityTypesRepository.Table.Where(x => x.CommodityTypeName == category).Count();
                // 判断要创建的商品类型不存在
                if (CommodityType == 0)
                {
                    // 创建新商品类型
                    var commodityType = new CommodityTypes
                    {
                        CommodityTypeName = category,
                        CommodityPid = commodityPid,
                        CommodityLevel = commodityLevel
                    };
                    // 商品类型插入
                    _commodityTypesRepository.Insert(commodityType);
                    return JsonHelper.Serialize(new
                    {
                        Data = commodityType,
                        Meta = new
                        {
                            Msg = "创建新商品类型成功！",
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 商品类型已经存在了
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = "该商品类型已存在，无法重复创建！",
                            Status = 400
                        }
                    });
                }
            }
        }

        // 商品类型信息修改
        [HttpPut("{id}")]
        public dynamic ModCommodityType(int id, NewCommodityType model)
        {
            var commodityType = _commodityTypesRepository.GetById(id);
            // 判断所修改商品类型Id是否存在
            if (commodityType != null)
            {
                // 商品类型名称
                commodityType.CommodityTypeName = model.CommodityTypeName;
                // 统计要修改的商品类型名称在商品类型表中的数量
                var commodityTypesCount = _commodityTypesRepository.Table.Where(x => x.CommodityTypeName == model.CommodityTypeName).ToList().Count();
                // 判断要修改的商品类型名称是否已经创建过了
                if (commodityTypesCount == 0)
                {
                    // 要修改的名称未创建
                    // 商品类型更新
                    _commodityTypesRepository.Update(commodityType);
                    return JsonHelper.Serialize(new
                    {
                        Data = commodityType,
                        Meta = new
                        {
                            Msg = string.Format("你修改的商品类型的id为: {0} ，已经修改成功，请注意查收！", id),
                            Status = 200
                        }
                    });
                }
                else
                {
                    // 要修改的名称已创建
                    return JsonHelper.Serialize(new
                    {
                        Data = "",
                        Meta = new
                        {
                            Msg = string.Format("你修改的商品类型的id为: {0} ，要修改的商品类型名称已存在，请尝试修改为其他名称！", id),
                            Status = 400
                        }
                    });
                }

            }
            else
            {
                // 该商品类型Id不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品类型不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 商品类型状态可用信息修改
        [HttpPut("status/{id}"), Route("status")]
        public dynamic ModStatus(int id)
        {
            var commodityType = _commodityTypesRepository.GetById(id);
            // 判断对应Id的商品类型是否存在
            if (commodityType != null)
            {
                // 指定Id的商品类型存在
                _commodityTypesRepository.ActiveStatus(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = string.Format("你修改的状态对应商品类型的id为: {0} ，已经修改成功，请注意查收！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的商品类型不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品类型不存在，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }

        // 商品类型信息删除（伪删除）
        [HttpDelete("{id}")]
        public dynamic DeleteCommodityType(int id)
        {
            var commodityType = _commodityTypesRepository.GetById(id);
            // 判断对应Id的商品类型是否存在
            if (commodityType != null)
            {
                // 对应Id的商品类型存在
                // 伪删除操作
                _commodityTypesRepository.PseudoDeletion(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = string.Format("你删除的商品类型Id为 {0} 的数据,伪删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 对应Id的商品类型不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的商品类型Id为 {0} 的数据,执行伪删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 商品类型信息删除（彻底删除）
        [HttpDelete("delete/{id}"), Route("delete")]
        public dynamic CompletelyDeleteCommodityType(int id)
        {
            var commodityType = _commodityTypesRepository.GetById(id);
            // 判断对应Id的商品类型是否存在
            if (commodityType != null)
            {   // 指定Id的商品类型存在
                // 彻底删除
                _commodityTypesRepository.Delete(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = string.Format("你删除的Id为 {0} 的商品类型数据,彻底删除成功！", id),
                        Status = 200
                    }
                });
            }
            else
            {
                // 指定Id的商品类型不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = string.Format("找不到的Id为 {0} 的商品类型数据,无法执行彻底删除操作,", id),
                        Status = 400
                    }
                });
            }
        }

        // 恢复被删除的商品类型数据
        [HttpDelete("recovery/{id}"), Route("recovery")]
        public dynamic RecoveryCommodityType(int id)
        {
            var commodityType = _commodityTypesRepository.GetById(id);
            // 判断对应Id的商品类型是否存在
            if (commodityType != null)
            {
                // 指定Id的商品类型存在
                // 恢复数据
                _commodityTypesRepository.Recovery(id);
                return JsonHelper.Serialize(new
                {
                    Data = commodityType,
                    Meta = new
                    {
                        Msg = string.Format("你恢复的商品类型的id为: {0} ，已经恢复成功，请注意查收！", id),
                        Status = 200
                    }

                });
            }
            else
            {
                // 指定Id的商品类型不存在
                return JsonHelper.Serialize(new
                {
                    Data = "",
                    Meta = new
                    {
                        Msg = "指定Id的商品类型不存在，恢复错误，请确认后重试！",
                        Status = 400
                    }
                });
            }
        }
    }
}
